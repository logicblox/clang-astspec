grammar ASTSpec;

@header{
package parser;

import errormessage.ErrorReporter;
import symboltable.SymbolTable;
}

@parser::members{

    private int semErrors = 0;
    
    public void newSemError(){
        this.semErrors++;
    }
    
    @Override
    public int getNumberOfSyntaxErrors(){
        return this.semErrors + super.getNumberOfSyntaxErrors();
    }
         
}


//spec parser
specification
@init{
      ErrorReporter reporter = ErrorReporter.getInstance();
      reporter.setFilename(this.getSourceName());
      reporter.setParser(this);
}
    : AST_SPEC IDENTIFIER SEMI_COLON
      ast_element_definitions
      EOF
    ;


//Elements from clang AST
ast_element_definitions
    : ( node_entity_definition
      | enumeration_definition
      )+
    ;

//Describing a node inside the clang AST and its properties
node_entity_definition
    : (ANNOTATION_NODE | ANNOTATION_ENTITY) L_PAR NAME_VALUE EQUALS thisName = IDENTIFIER
                            (COMMA TARGET_TYPE EQUALS targetType = STRING_LITERAL)?
                            (COMMA CACHE EQUALS cache = BOOL_LITERAL)?
                      R_PAR
      name = IDENTIFIER (EXTENDS baseName = IDENTIFIER)? L_SQBR
        node_definition_content_element 
      R_SQBR
    ;

node_definition_content_element
    : ( constructor_definition
      | property_definition
      )*
    ;

constructor_definition
    : ANNOTATION_CONSTRUCTOR L_PAR identifier_list R_PAR
    ;

property_definition
    : ANNOTATION_PROPERTY L_PAR PR_TYPE EQUALS PR_TYPE_VALUE
                                (COMMA TARGET_TYPE EQUALS targetType = STRING_LITERAL)?
                                (COMMA DEREF EQUALS needsDeref = BOOL_LITERAL)?
                                (COMMA CONDITION EQUALS cond = STRING_LITERAL)?
                                (COMMA ORDER EQUALS order = BOOL_LITERAL)?
                          R_PAR
      IDENTIFIER L_PAR parameter? R_PAR L_SQBR
        clang_code_for_property+
      R_SQBR
    ;

clang_code_for_property
    : ( refStart = PROPERTY_REFERENCE_START 
      | refEnd = PROPERTY_REFERENCE_END 
      | ref = PROPERTY_REFERENCE) EQUALS STRING_LITERAL
      | STRING_LITERAL
    ;

//Enums
enumeration_definition
    : ANNOTATION_ENUMERATION (L_PAR PROPERTY_PREFIX EQUALS prefix = STRING_LITERAL R_PAR)?
      name = IDENTIFIER L_SQBR
        enumerator += IDENTIFIER (COMMA enumerator+= IDENTIFIER)*
      R_SQBR
    ;

//Parameters for language's different constracts
//REVIEW: Do we need more complex types ??
parameter_list
    : parameter (COMMA parameter)*
    ;

parameter
    : type = IDENTIFIER name = IDENTIFIER
    ;

//List of IDENTIFIERS for constructors
identifier_list
    : ids += IDENTIFIER (COMMA ids += IDENTIFIER)*
    ;

//end spec parser

//auxiliary file parser

aux_file
    : list_lang_annotations
    ;

list_lang_annotations
    : lang_annotations+
    ;

lang_annotations
    : LANG_ANNOTATIONS CODE_SEGMENT
    ;

//end auxiliary file parser

//lexer

LANG_ANNOTATIONS
    : '@header'
    | '@members'
    ;

CODE_SEGMENT : '{' ( CODE_SEGMENT | ~[{}] )* '}' ;

AST_SPEC : 'ast_spec' ;

EXTENDS
    : 'extends'
    ;

NAME_VALUE
    : 'name'
    ;

PR_TYPE
    : 'type'
    ;

TARGET_TYPE
    : 'targetType'
    ;

PR_TYPE_VALUE
    : 'many-to-one'
    | 'one-to-many'
    | 'one-to-one'
    | 'many-to-many'
    | 'existential'
    ;

EQUALS
    : '='
    ;

ANNOTATION_ENUMERATION
    : '@enum'
    ;

ANNOTATION_PROPERTY
    : '@property'
    ;

ANNOTATION_NODE
    : '@node' 
    ;

ANNOTATION_ENTITY
    : '@entity'
    ;

ANNOTATION_CONSTRUCTOR
    : '@constructor'
    ;

L_PAR
    : '('
    ;

R_PAR
    : ')'
    ;

DEREF
    : 'deref'
    ;

CONDITION
    : 'cond'
    ;

ORDER
    : 'ordered'
    ;

BOOL_LITERAL
    : 'true'
    | 'false'
    ;

PROPERTY_PREFIX
    : 'prefix'
    ;

CACHE
    : 'cache'
    ;

IDENTIFIER
    : [a-zA-Z_] ([a-zA-Z_0-9])*
    ;

PROPERTY_REFERENCE_START
    : '$'[a-zA-Z_] ([a-zA-Z_0-9])*':start'
    ;

PROPERTY_REFERENCE_END
    : '$'[a-zA-Z_] ([a-zA-Z_0-9])*':end'
    ;

PROPERTY_REFERENCE
    : '$'[a-zA-Z_] ([a-zA-Z_0-9])*
    ;

STRING_LITERAL
    : '"' ANY_TOKEN*? '"'
    ;

L_CURL
    : '{'
    ;

R_CURL
    : '}'
    ;

SEMI_COLON
    : ';'
    ;

COMMA
    : ','
    ;

L_SQBR
    : '['
    ;

R_SQBR
    : ']'
    ;

WS 
    : [ \t\n\r]+ -> skip
    ;

COMMENT
    : ('/*' .*? '*/'
    | '//' .*? (('\r'?'\n') | EOF)) -> skip
    ;

/*
 * For now leave a catch all rule in lexer.
 * TODO: List all valid charactrers for code segments
 */
ANY_TOKEN
    : .
    ;