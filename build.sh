set -ex
CLASSPATH=/usr/share/java/antlr4-runtime-4.5.1.jar:/usr/share/java/args4j.jar:/usr/share/java/jgrapht0.8.jar

mkdir -p classes
javac -cp $CLASSPATH -d classes `find . -name '*.java'`

cd classes
echo "Class-Path: antlr4-runtime-4.5.1.jar args4j.jar jgrapht0.8.jar" > Manifest
jar cfme clang-astspec.jar Manifest clangAST.Driver `find -name '*.class'`

mkdir -p ../lib
mv clang-astspec.jar ../lib

ln -sf /usr/share/java/antlr4-runtime-4.5.1.jar ../lib/antlr4-runtime-4.5.1.jar
ln -sf /usr/share/java/args4j.jar ../lib/args4j.jar
ln -sf /usr/share/java/jgrapht0.8.jar ../lib/jgrapht.jar
