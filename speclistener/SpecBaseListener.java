package speclistener;

import symboltable.ASTNode;
import symboltable.Constructor;
import symboltable.Entity;
import symboltable.EntityProperty;
import symboltable.EnumProperty;
import symboltable.Enumeration;
import symboltable.ExistentialProperty;
import symboltable.IntProperty;
import symboltable.NodeProperty;
import symboltable.Property;
import symboltable.StringProperty;
import visitor.forproperties.EnterVisitor;

/**
 *
 * @author kostasferles
 */
public class SpecBaseListener implements SpecListener{
    
    private EnterVisitor enter = new EnterVisitor();
    
    @Override
    public void enter(){
        
    }

    @Override
    public void enterProperties(){
        
    }
    
    @Override
    public void enterProperty(Property p){
        p.accept(enter, this);
    }
    
    @Override
    public void enterStringProperty(StringProperty s) {
        
    }
    
    @Override
    public void enterIntProperty(IntProperty ip){
        
    }

    @Override
    public void enterEnumProperty(EnumProperty e) {
        
    }

    @Override
    public void enterNodeProperty(NodeProperty n) {
        
    }
    
    @Override
    public void enterEntityProperty(EntityProperty ep){
    
    }
    
    @Override
    public void enterExistentialProperty(ExistentialProperty ep){
        
    }
    
    @Override
    public void exitProperties(){
        
    }
    
    @Override
    public void enterAstNodes(){
        
    }

    @Override
    public void enterAstNode(ASTNode n) {
        
    }

    @Override
    public void exitAstNode(ASTNode n) {
        
    }
    
    @Override
    public void exitAstNodes(){
        
    }
    
    @Override
    public void enterEntities(){
        
    }
    
    @Override
    public void enterEntity(Entity en){
        
    }
    
    @Override
    public void exitEntity(Entity en){
        
    }
    
    @Override
    public void exitEntities(){
        
    }
    
    @Override
    public void enterEnumerations(){
        
    }

    @Override
    public void enterEnumNode(Enumeration enumeration) {
        
    }
    
    @Override
    public void exitEnumerations(){
        
    }

    @Override
    public void enterConstructor(Constructor cons) {
        
    }
    
    @Override
    public void exit(){
        
    }
    
}
