package speclistener;

import symboltable.ASTNode;
import symboltable.Constructor;
import symboltable.Entity;
import symboltable.EntityProperty;
import symboltable.EnumProperty;
import symboltable.Enumeration;
import symboltable.ExistentialProperty;
import symboltable.IntProperty;
import symboltable.NodeProperty;
import symboltable.Property;
import symboltable.StringProperty;

/**
 *
 * @author kostasferles
 */
public interface SpecListener {
    
    void enter();
    
    void enterProperties();
    
    void enterProperty(Property p);
    
    void enterStringProperty(StringProperty s);
    
    void enterIntProperty(IntProperty ip);
    
    void enterEnumProperty(EnumProperty e);
    
    void enterNodeProperty(NodeProperty n);
    
    void enterEntityProperty(EntityProperty ep);
    
    void enterExistentialProperty(ExistentialProperty ep);
    
    void exitProperties();
    
    void enterAstNodes();
    
    void enterAstNode(ASTNode n);
    
    void exitAstNode(ASTNode n);
    
    void exitAstNodes();
    
    void enterEntities();
    
    void enterEntity(Entity en);
    
    void exitEntity(Entity en);
    
    void exitEntities();
    
    void enterEnumerations();
    
    void enterEnumNode(Enumeration enumeration);
    
    void exitEnumerations();
    
    void enterConstructor(Constructor cons);
    
    void exit();
    
}
