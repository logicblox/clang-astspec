package visitor.forproperties;

import symboltable.EntityProperty;
import symboltable.EnumProperty;
import symboltable.ExistentialProperty;
import symboltable.IntProperty;
import symboltable.NodeProperty;
import symboltable.StringProperty;

/**
 *
 * @author kostasferles
 */
public interface PropertyVisitorRvAndArg<R, A> {

    R visit(StringProperty sp, A arg);
    
    R visit(EnumProperty enp, A arg);
    
    R visit(NodeProperty np, A arg);
    
    R visit(EntityProperty ep, A arg);
    
    R visit(IntProperty ip, A arg);
    
    R visit(ExistentialProperty ep, A arg);
}
