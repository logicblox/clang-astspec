package visitor.forproperties;

import symboltable.EntityProperty;
import symboltable.EnumProperty;
import symboltable.ExistentialProperty;
import symboltable.IntProperty;
import symboltable.NodeProperty;
import symboltable.StringProperty;

/**
 *
 * @author kostasferles
 */
public interface PropertyVisitorArg<A> {
    
    void visit(StringProperty sp, A arg);
    
    void visit(EnumProperty enp, A arg);
    
    void visit(NodeProperty np, A arg);
    
    void visit(EntityProperty ep, A arg);
    
    void visit(IntProperty ip, A arg);
    
    void visit(ExistentialProperty ep, A arg);
}
