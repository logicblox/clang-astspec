package visitor.forproperties;

import speclistener.SpecListener;
import symboltable.EntityProperty;
import symboltable.EnumProperty;
import symboltable.ExistentialProperty;
import symboltable.IntProperty;
import symboltable.NodeProperty;
import symboltable.StringProperty;

/**
 *
 * @author kostasferles
 */
public class EnterVisitor implements PropertyVisitorArg<SpecListener>{

    @Override
    public void visit(StringProperty sp, SpecListener arg) {
        arg.enterStringProperty(sp);
    }

    @Override
    public void visit(EnumProperty enp, SpecListener arg) {
        arg.enterEnumProperty(enp);
    }

    @Override
    public void visit(NodeProperty np, SpecListener arg) {
        arg.enterNodeProperty(np);
    }
    
    @Override
    public void visit(EntityProperty ep, SpecListener arg){
        arg.enterEntityProperty(ep);
    }
    
    @Override
    public void visit(IntProperty ip, SpecListener arg){
        arg.enterIntProperty(ip);
    }
    
    @Override
    public void visit(ExistentialProperty ep, SpecListener arg){
        arg.enterExistentialProperty(ep);
    }
    
}
