package visitor.forproperties;

import symboltable.EntityProperty;
import symboltable.EnumProperty;
import symboltable.ExistentialProperty;
import symboltable.IntProperty;
import symboltable.NodeProperty;
import symboltable.StringProperty;

/**
 *
 * @author kostasferles
 */
public interface PropertyVisitor {
    
    void visit(StringProperty sp);
    
    void visit(EnumProperty enp);
    
    void visit(NodeProperty np);
    
    void visit(EntityProperty ep);
    
    void visit(IntProperty ip);
    
    void visit(ExistentialProperty ep);

}
