package visitor.forContainsProperties;

import symboltable.ASTNode;
import symboltable.Entity;

/**
 *
 * @author kostasferles
 */
public interface ContainsPropertiesVisitor {

    public void visit(ASTNode n);
    
    public void visit(Entity en);
}
