package visitor.forContainsProperties;

import symboltable.ASTNode;
import symboltable.Entity;

/**
 *
 * @author kostasferles
 */
public interface ContainsPropertiesVisitorRv<R> {

    public R visit(ASTNode n);
    
    public R visit(Entity en);
    
}
