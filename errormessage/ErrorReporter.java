package errormessage;

import org.antlr.v4.runtime.Token;
import parser.ASTSpecParser;

/**
 *
 * @author kostasferles
 */
public final class ErrorReporter {

    private final static ErrorReporter instance = new ErrorReporter();
    
    private String filename;
    
    private ASTSpecParser p;
    
    private ErrorReporter(){
    
    }
    
    public static ErrorReporter getInstance(){
        return instance;
    }
    
    public void setFilename(String filename){
        this.filename = filename;
    }
    
    public void setParser(ASTSpecParser p){
        this.p = p;
    }
    
    public void reportError(Token t, String message){
        p.newSemError();
        System.err.println(this.filename + ":" + t.getLine() + ":" + t.getCharPositionInLine() + ": " + " error: " + message);
    }
}
