package errormessage;

/**
 *
 * @author kostasferles
 */
public class ErrorMessage extends Exception {

    public ErrorMessage(String message){
        super(message);
    }
}
