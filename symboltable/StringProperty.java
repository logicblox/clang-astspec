package symboltable;

import visitor.forproperties.PropertyVisitor;
import visitor.forproperties.PropertyVisitorArg;
import visitor.forproperties.PropertyVisitorRvAndArg;

/**
 *
 * @author kostasferles
 */
public class StringProperty extends Property {

    String valueName;

    public StringProperty(String name, ContainsProperties parent, Type type, String valueName){
        super(name, parent, type, valueName);
    }
    
    @Override
    public String getRangeTypeName() {
        return "string";
    }

    @Override
    public <R, A> R accept(PropertyVisitorRvAndArg<R, A> v, A arg) {
        return v.visit(this, arg);
    }
    
    @Override
    public <A> void accept(PropertyVisitorArg<A> v, A arg) {
        v.visit(this, arg);
    }

    @Override
    public void accept(PropertyVisitor v) {
        v.visit(this);
    }

}
