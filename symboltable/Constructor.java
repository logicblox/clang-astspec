package symboltable;

import errormessage.ErrorMessage;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author kostasferles
 */
public class Constructor {
    
    private ContainsProperties parent;
    
    private List<Property> properties = new ArrayList<>();
    
    public Constructor(ContainsProperties parent){
        this.parent = parent;
    }
    
    public Constructor(ContainsProperties parent, Constructor supersCons){
        this.parent = parent;
        
        for(Property p : supersCons.properties)
            this.properties.add(p);
    }

    public void insertProperty(Property property) throws ErrorMessage{
        if(this.properties.contains(property))
            throw new ErrorMessage("constructor referencecs a property multiple times.");
        
        Property.Type pType = property.getType();
        
        if(!(pType == Property.Type.OneToOne || pType == Property.Type.ManyToOne) || property.isOptional())
            throw new ErrorMessage("invalid property '" + property.getName() + "' for constructor, only functional properties can participate on constructors");
        this.properties.add(property); 
    }

    public Iterator<Property> getProperties(){
        return this.properties.iterator();
    }
    
    public int getNumOfProperties(){
        return properties.size();
    }
    
    public ContainsProperties getParent(){
        return this.parent;
    }
    
    public boolean containsProperty(Property p){
        return this.properties.contains(p);
    }
    
}
