package symboltable;

/**
 *
 * @author kostasferles
 */
public class ExprForASTElement {

    private String code;

    public ExprForASTElement(String code){
        this.code = code;
    }

    public String getCodeForElement(){
        return this.code;
    }
}
