package symboltable;

import visitor.forproperties.PropertyVisitor;
import visitor.forproperties.PropertyVisitorArg;
import visitor.forproperties.PropertyVisitorRvAndArg;

/**
 *
 * @author kostasferles
 */
public class ExistentialProperty extends Property{

    public ExistentialProperty(String name, ContainsProperties parent){
        super(name, parent, Type.Existential, "");
    }
    
    @Override
    public String getRangeTypeName() {
        return "";
    }

    @Override
    public <R, A> R accept(PropertyVisitorRvAndArg<R, A> v, A arg) {
        return v.visit(this, arg);
    }

    @Override
    public <A> void accept(PropertyVisitorArg<A> v, A arg) {
        v.visit(this, arg);
    }

    @Override
    public void accept(PropertyVisitor v) {
        v.visit(this);
    }
    
}
