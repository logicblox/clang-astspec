package symboltable;

import errormessage.ErrorMessage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 * @author kostasferles
 */
public class SymbolTable {

    private final static SymbolTable instance = new SymbolTable();
    
    private SymbolTable(){
    
    }
    
    private String clangSpecName;
    
    private List<ASTNode> nodes_ordered = new ArrayList<>();

    private Map<String, ASTNode> nodes = new HashMap<>();

    private List<Enumeration> enums_ordered = new ArrayList<>();
    
    private Map<String, Enumeration> enums = new HashMap<>();
    
    private List<Entity> entities_ordered = new ArrayList<>();
    
    private Map<String, Entity> entities = new HashMap<>();

    private AuxCodeSegments segments = new AuxCodeSegments();

    public static SymbolTable getInstance(){
        return instance;
    }

    public void insertNode(ASTNode node) throws ErrorMessage{
        String name = node.getName();
        if(this.nodes.containsKey(name))
            throw new ErrorMessage("redeclaration of node " + name);
        if(this.enums.containsKey(name) || this.entities.containsKey(name))
            throw new ErrorMessage(name + "is declared as a different type of symbol");
        this.nodes.put(name, node);
        this.nodes_ordered.add(node);
    }
    
    public void insertEntity(Entity en) throws ErrorMessage{
        String name = en.getName();
        if(this.entities.containsKey(name))
            throw new ErrorMessage("redeclaration of entity " + name);
        if(this.nodes.containsKey(name) || this.enums.containsKey(name))
            throw new ErrorMessage(name + "is declared as a different type of symbol");
        this.entities.put(name, en);
        this.entities_ordered.add(en);
    }

    public void insertEnum(Enumeration enumeration) throws ErrorMessage{
        String name = enumeration.getName();
        if(this.enums.containsKey(name))
            throw new ErrorMessage("redeclaration of enumeration " + name);
        if(this.nodes.containsKey(name) || this.entities.containsKey(name))
            throw new ErrorMessage(name + "is declared as a different type of symbol");
        this.enums.put(name, enumeration);
        this.enums_ordered.add(enumeration);
    }

    public ASTNode getNode(String name){
        return this.nodes.get(name);
    }
    
    public Entity getEntity(String name){
        return this.entities.get(name);
    }

    public Enumeration getEnumeration(String name){
        return this.enums.get(name);
    }
    
    public void insertMembers(String segment){
        this.segments.addMembers(segment);
    }
    
    public void insertHeader(String segment){
        this.segments.addHeader(segment);
    }
    
    public String getMembers(){
        return this.segments.getMembers();
    }
    
    public String getHeader(){
        return this.segments.getHeader();
    }
    
    public Iterator<ASTNode> getNodes(){
        return this.nodes_ordered.iterator();
    }
    
    public Iterator<Enumeration> getEnumerations(){
        return this.enums_ordered.iterator();
    }
    
    public Iterator<Entity> getEntities(){
        return this.entities_ordered.iterator();
    }
    
    public void setClangSpecName(String clangSpecName){
        this.clangSpecName = clangSpecName;
    }
    
    public String getClangSpecName(){
        return this.clangSpecName;
    }

}
