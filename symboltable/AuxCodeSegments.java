package symboltable;

/**
 *
 * @author kostasferles
 */
public class AuxCodeSegments {

    private String header;
    
    private String members;
    
    public void addHeader(String header){
        this.header = header;
    }
    
    public void addMembers(String members){
        this.members = members;
    }
    
    public String getHeader(){
        return this.header;
    }
    
    public String getMembers(){
        return this.members;
    }
}
