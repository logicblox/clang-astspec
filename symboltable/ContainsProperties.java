package symboltable;

import errormessage.ErrorMessage;
import java.util.Collection;
import visitor.forContainsProperties.ContainsPropertiesVisitor;
import visitor.forContainsProperties.ContainsPropertiesVisitorRv;

/**
 *
 * @author kostasferles
 */
//REVIEW: This class could be an abstract class
public interface ContainsProperties {

    public String getName();
    
    public String getThisName();
    
    public void setTargetLangType(String type);
    
    public String getTargetLangType();
    
    public void insertProperty(Property p) throws ErrorMessage;
    
    public Property getProperty(String name);
    
    public Collection<Property> getProperties();
    
    public void insertConstructor(Constructor cons) throws ErrorMessage;
    
    public Constructor getConstructor();
    
    public void setHasSubClasses();
    
    public boolean hasSubClasses();
    
    public ContainsProperties getSuper();
    
    public void accept(ContainsPropertiesVisitor visitor);
    
    public <R> R accept(ContainsPropertiesVisitorRv<R> visitor);
}
