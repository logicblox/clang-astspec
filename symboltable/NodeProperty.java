package symboltable;

import visitor.forproperties.PropertyVisitor;
import visitor.forproperties.PropertyVisitorArg;
import visitor.forproperties.PropertyVisitorRvAndArg;

/**
 *
 * @author kostasferles
 */
public class NodeProperty extends Property {

    private ASTNode node;

    public NodeProperty(String name, ContainsProperties parent, Type type, String valueName, ASTNode rangeNode){
        super(name, parent, type, valueName);
        this.node = rangeNode;
    }
    
    public ASTNode getRangeNode(){
        return this.node;
    }
    
    @Override
    public String getRangeTypeName() {
        return this.node.getName();
    }

    @Override
    public <R, A> R accept(PropertyVisitorRvAndArg<R, A> v, A arg) {
        return v.visit(this, arg);
    }

    @Override
    public <A> void accept(PropertyVisitorArg<A> v, A arg) {
        v.visit(this, arg);
    }

    @Override
    public void accept(PropertyVisitor v) {
        v.visit(this);
    }
    
}
