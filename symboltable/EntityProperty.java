package symboltable;

import visitor.forproperties.PropertyVisitor;
import visitor.forproperties.PropertyVisitorArg;
import visitor.forproperties.PropertyVisitorRvAndArg;

/**
 *
 * @author kostasferles
 */
//REVIEW: Should we coarse this class with NodeProperty?
public class EntityProperty extends Property{

    private Entity rangeEntity;
    
    public EntityProperty(String name, ContainsProperties parent, Type type, String valueName, Entity rangeEntity){
        super(name, parent, type, valueName);
        this.rangeEntity = rangeEntity;
    }
    
    @Override
    public String getRangeTypeName() {
        return this.rangeEntity.getName();
    }
    
    public Entity getRangeEntity(){
        return this.rangeEntity;
    }

    @Override
    public <R, A> R accept(PropertyVisitorRvAndArg<R, A> v, A arg) {
        return v.visit(this, arg);
    }

    @Override
    public <A> void accept(PropertyVisitorArg<A> v, A arg) {
        v.visit(this, arg);
    }

    @Override
    public void accept(PropertyVisitor v) {
        v.visit(this);
    }

    
}
