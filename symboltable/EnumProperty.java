package symboltable;

import visitor.forproperties.PropertyVisitor;
import visitor.forproperties.PropertyVisitorArg;
import visitor.forproperties.PropertyVisitorRvAndArg;

/**
 *
 * @author kostasferles
 */
public class EnumProperty extends Property {

    private Enumeration enumeration;
    
    public EnumProperty(String name, ContainsProperties parent, Type type, String valueName, Enumeration enumeration){
        super(name, parent, type, valueName);
        this.enumeration = enumeration;
    }
    
    public Enumeration getEnumeration(){
        return this.enumeration;
    }

    @Override
    public String getRangeTypeName() {
        return this.enumeration.getName();
    }
    
    @Override
    public <R, A> R accept(PropertyVisitorRvAndArg<R, A> v, A arg) {
        return v.visit(this, arg);
    }

    @Override
    public <A> void accept(PropertyVisitorArg<A> v, A arg) {
        v.visit(this, arg);
    }

    @Override
    public void accept(PropertyVisitor v) {
        v.visit(this);
    }
    
    
}
