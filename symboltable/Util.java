package symboltable;

import errormessage.ErrorMessage;

/**
 *
 * @author kostasferles
 */
public class Util {
    
    private static SymbolTable table = SymbolTable.getInstance();
    
    public static Property.Type stringToType(String type){
        switch(type){
            case "many-to-one":
                return Property.Type.ManyToOne;
            case "one-to-many":
                return Property.Type.OneToMany;
            case "one-to-one":
                return Property.Type.OneToOne;
            case "many-to-many":
                return Property.Type.ManyToMany;
            case "existential":
                return Property.Type.Existential;
            default:
                assert false;
        }
        return null;
    }
    
    public static String unwrapString(String s){
        return s.substring(1, s.length() - 1);
    }
}
