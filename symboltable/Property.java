package symboltable;

import errormessage.ErrorMessage;
import visitor.forproperties.PropertyVisitor;
import visitor.forproperties.PropertyVisitorArg;
import visitor.forproperties.PropertyVisitorRvAndArg;

/**
 *
 * @author kostasferles
 */
public abstract class Property {

    private class IteratorCommands {
        
        ExprForASTElement start;
        
        ExprForASTElement end;
    
    }
    
    private String name;

    private Type type;

    private String valueName;
    
    private ExprForASTElement exprForVal;
    
    private IteratorCommands iters;
    
    private ContainsProperties parent;
    
    private String targetLangType;
    
    private String cond;
    
    boolean needsDeref = false;
    
    boolean isOrdered = false;

    private void checkValueName(String valueName) throws ErrorMessage{
        if(!this.valueName.equals(valueName))
            throw new ErrorMessage("unknown property value " + valueName + " in property " + this.name);
    }
    
    public enum Type{
        OneToOne,
        OneToMany,
        ManyToOne,
        ManyToMany,
        Existential
    }

    public Property(String name, ContainsProperties parent, Type type, String valueName){
        this.name = name;
        this.parent = parent;
        this.type = type;
        this.valueName = valueName;
    }

    public String getName(){
        return this.name;
    }

    public Type getType(){
        return this.type;
    }
    
    public void setCommandForValue(String valueName, ExprForASTElement exprVal) throws ErrorMessage{
        checkValueName(valueName);
        if(this.iters != null)
            throw new ErrorMessage("setting expression for both iterators and simple values in property '" + name + "'");
        this.exprForVal = exprVal;
    }

    public void setCommandStart(String valueName, ExprForASTElement exprStart) throws ErrorMessage{
        assert this.type == Type.OneToMany || this.type == Type.ManyToMany;
        checkValueName(valueName);
        if(this.exprForVal != null)
            throw new ErrorMessage("setting expression for both iterators and simple values in property '" + name + "'");
        if(this.iters == null){
            this.iters = new IteratorCommands();
        }
        else{
            if(iters.start != null)
                throw new ErrorMessage("setting expr for iterator '" + valueName + ":start' multiple times");
        }
        this.iters.start = exprStart;
    }
    
    public void setCommandEnd(String valueName, ExprForASTElement exprEnd) throws ErrorMessage{
        assert this.type == Type.OneToMany || this.type == Type.ManyToMany;
        checkValueName(valueName);
        if(this.exprForVal != null)
            throw new ErrorMessage("setting expression for both iterators and simple values in property '" + name + "'");
        if(this.iters == null){
            this.iters = new IteratorCommands();
        }
        else{
            if(iters.end != null)
                throw new ErrorMessage("setting expr for iterator '" + valueName + ":end' multiple times");
        }
        this.iters.end = exprEnd;
    }

    public ExprForASTElement getExprStart(){
        assert iters != null;
        return this.iters.start;
    }

    public ExprForASTElement getExprEnd(){
        assert iters != null;
        return this.iters.end;
    }
    
    public ExprForASTElement getExprForValue(){
        assert this.exprForVal != null;
        return this.exprForVal;
    }
    
    public String getValueName(){
        return this.valueName;
    }
    
    public boolean hasIterators(){
        return this.iters != null;
    }

    public void checkCommandsForProperty() throws ErrorMessage{
        switch(this.type){
            case OneToOne:
            case ManyToOne:
            case Existential:
                if(this.exprForVal == null)
                    throw new ErrorMessage("missing expression for value in property '" + name + "'");
                if(this.needsDeref)
                    throw new ErrorMessage("invalid dereference information for property " + this.name);
                if(this.isOrdered)
                    throw new ErrorMessage("functional predicate '" + this.name + "' can not be ordered");
                break;
            case OneToMany:
            case ManyToMany:
                if(iters != null){
                    if(iters.start == null)
                        throw new ErrorMessage("missing iterator start in property '" + name + "'");
                    if(iters.end == null)
                        throw new ErrorMessage("missing iterator end in property '" + name + "'");
                }
                else{
                    if(exprForVal == null)
                        throw new ErrorMessage("missing expression for value in property '" + name + "'");
                    if(isOrdered)
                        throw new ErrorMessage("ordered predicate '" + name + "' must provide iterators for their values");
                }
                break;
        }
    }
    
    public ContainsProperties getParent(){
        return this.parent;
    }
    
    public void setTargetLangType(String  targetLangType){
        this.targetLangType = targetLangType;
    }
    
    public  String getTargetLangType(){
        return this.targetLangType;
    }
    
    public void setDeref(){
        this.needsDeref = true;
    }
    
    public void setOrdered(){
        this.isOrdered = true;
    }
    
    public void setCond(String condition){
        this.cond = condition;
    }
    
    public String getCond(){
        return this.cond;
    }
    
    public boolean isOptional(){
        return this.cond != null;
    }
    
    public boolean needsDeref(){
        return this.needsDeref;
    }
    
    public boolean isOrdered(){
        return this.isOrdered;
    }
    
    public abstract String getRangeTypeName();
    
    public abstract <R, A> R accept(PropertyVisitorRvAndArg<R, A> v, A arg);
    
    public abstract <A> void accept(PropertyVisitorArg<A> v, A arg);
    
    public abstract void accept(PropertyVisitor v);
}
