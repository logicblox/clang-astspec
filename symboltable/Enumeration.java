package symboltable;

import errormessage.ErrorMessage;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author kostasferles
 */
public class Enumeration {

    private String name;

    private String prefix;
    
    private List<String> enumerators = new ArrayList<>();

    public Enumeration(String name){
        this.name = name;
    }

    public void addEnumerator(String enumerator) throws ErrorMessage{
        if(this.enumerators.contains(enumerator))
            throw new ErrorMessage("enumerator " + enumerator + "already exists inside" + this.name);
        this.enumerators.add(enumerator);
    }
    
    public String getName(){
        return this.name;
    }

    public String getEnumerator(int index){
        return this.enumerators.get(index);
    }
    
    public int getNumOfEnumerator(){
        return this.enumerators.size();
    }
    
    public Iterator<String> getEnumerators(){
        return this.enumerators.iterator();
    }

    public int getEnumeratorsIndex(String enumerator){
        return this.enumerators.indexOf(enumerator);
    }
    
    public void setPrefix(String prefix){
        this.prefix = prefix;
    }
    
    public String getPrefix(){
        return this.prefix;
    }
    
}
