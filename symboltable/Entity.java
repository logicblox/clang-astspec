package symboltable;

import errormessage.ErrorMessage;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import visitor.forContainsProperties.ContainsPropertiesVisitor;
import visitor.forContainsProperties.ContainsPropertiesVisitorRv;

/**
 *
 * @author kostasferles
 */
public class Entity implements ContainsProperties{

    private String name;
    
    private String thisName;
    
    private String targetLangType;
    
    private Entity parentEntity;
    
    private Map<String, Property> properties = new HashMap<>();
    
    private Constructor constructor;
    
    private boolean hasSubClasses = false;
    
    public Entity(String name, String thisName){
        this.name = name;
        this.thisName = thisName;
    }
    
    public Entity(String name, String thisName, Entity parentEntity){
        this.name = name;
        this.thisName = thisName;
        this.parentEntity = parentEntity;
        this.parentEntity.hasSubClasses = true;
    }
    
    @Override
    public String getName(){
        return this.name;
    }
    
    @Override
    public String getThisName(){
        return this.thisName;
    }
    
    @Override
    public void setTargetLangType(String type){
        this.targetLangType = type;
    }
    
    @Override
    public String getTargetLangType(){
        return this.targetLangType;
    }
    
    @Override
    public Entity getSuper(){
        return this.parentEntity;
    }
    
    @Override
    public void insertConstructor(Constructor cons) throws ErrorMessage{
        if(this.constructor != null)
            throw new ErrorMessage("multiple constructors in node declaration");
        this.constructor = cons;
    }
    
    @Override
    public void insertProperty(Property p) throws ErrorMessage{
        String propertyName = p.getName();
        if(this.properties.containsKey(propertyName))
            throw new ErrorMessage("redeclaration of property '" + propertyName + "' inside entity " + this.name);
        
        this.properties.put(propertyName, p);
    }
    
    @Override
    public Property getProperty(String name) {
        if(this.properties.containsKey(name))
            return this.properties.get(name);
        return this.parentEntity == null ? null : this.parentEntity.getProperty(name);
    }
    
    @Override
    public Constructor getConstructor(){
        if(this.constructor != null) return this.constructor;
        return this.parentEntity == null ? null : this.parentEntity.getConstructor();
    }
    
    @Override
    public Collection<Property> getProperties(){
        return this.properties.values();
    }
    
    @Override
    public void setHasSubClasses() {
        this.hasSubClasses = true;
    }
    
    @Override
    public boolean hasSubClasses(){
        return this.hasSubClasses;
    }
    
    @Override
    public void accept(ContainsPropertiesVisitor vis){
        vis.visit(this);
    }
    
    @Override
    public <R> R accept(ContainsPropertiesVisitorRv<R> vis){
        return vis.visit(this);
    }
    
    @Override
    public String toString(){
        return this.name;
    }
    
}
