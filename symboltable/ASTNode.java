package symboltable;

import errormessage.ErrorMessage;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import visitor.forContainsProperties.ContainsPropertiesVisitor;
import visitor.forContainsProperties.ContainsPropertiesVisitorRv;

/**
 *
 * @author kostasferles
 */
public class ASTNode implements ContainsProperties {

    private String name;

    private ASTNode parentNode;
    
    private Map<String, Property> properties = new HashMap<>();
    
    private String thisName;
    
    private String targetLangType;
    
    private boolean hasSubClasses = false;
    
    private boolean needsCache = false;
    
    Constructor constructor;

    public ASTNode(String name, String thisName){
        this.name = name;
        this.thisName = thisName;
    }

    public ASTNode(String name, String thisName, ASTNode parentNode){
        this.name = name;
        this.thisName = thisName;
        this.parentNode = parentNode;
        this.parentNode.hasSubClasses = true;
    }

    @Override
    public String getName(){
        return this.name;
    }

    @Override
    public ASTNode getSuper(){
        return this.parentNode;
    }

    @Override
    public void insertProperty(Property pr) throws ErrorMessage{
        String propertyName = pr.getName();
        if(this.properties.containsKey(propertyName))
            throw new ErrorMessage("redeclaration of property '" + propertyName + "' inside node " + this.name);
        
        this.properties.put(propertyName, pr);
    }
    
    @Override
    public Property getProperty(String name) {
        if(this.properties.containsKey(name))
            return this.properties.get(name);
        return this.parentNode == null ? null : this.parentNode.getProperty(name);
    }
    
    @Override
    public boolean hasSubClasses(){
        return this.hasSubClasses;
    }

    @Override
    public void insertConstructor(Constructor constructor) throws ErrorMessage{
        if(this.constructor != null)
            throw new ErrorMessage("multiple constructors in node declaration");
        this.constructor = constructor;
    }

    @Override
    public Constructor getConstructor(){
        if(this.constructor != null) return this.constructor;
        return this.parentNode == null ? null : this.parentNode.getConstructor();
    }

    @Override
    public void setHasSubClasses() {
        this.hasSubClasses = true;
    }
    
    @Override
    public Collection<Property> getProperties(){
        return this.properties.values();
    }
    
    @Override
    public String getThisName(){
        return this.thisName;
    }
    
    @Override
    public void setTargetLangType(String targetLangType){
        this.targetLangType = targetLangType;
    }
    
    @Override
    public String getTargetLangType(){
        return this.targetLangType;
    }
    
    public void setNeedsCache(){
        this.needsCache = true;
    }
    
    public boolean needsCache(){
        if(this.parentNode == null) 
            return this.needsCache;
        else
            if(!this.needsCache) return parentNode.needsCache();
            else return true;
    }
    
    @Override
    public void accept(ContainsPropertiesVisitor vis){
        vis.visit(this);
    }
    
    @Override
    public <R> R accept(ContainsPropertiesVisitorRv<R> vis){
        return vis.visit(this);
    }
    
    @Override
    public String toString(){
        return this.name;
    }
}
