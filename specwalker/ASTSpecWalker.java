package specwalker;

import java.util.Iterator;
import speclistener.SpecListener;
import symboltable.ASTNode;
import symboltable.Constructor;
import symboltable.ContainsProperties;
import symboltable.Entity;
import symboltable.Enumeration;
import symboltable.Property;
import symboltable.SymbolTable;

/**
 *
 * @author kostasferles
 */
public class ASTSpecWalker {
        
    private SpecListener listener;
    
    private SymbolTable table = SymbolTable.getInstance();
    
    private void traverseElementWithProps(ContainsProperties elem){
        Constructor cons = elem.getConstructor();

        if(cons.getParent() == elem){
            traverseConstructor(cons);
        }

        Iterator<Property> pIt = elem.getProperties().iterator();

        while(pIt.hasNext()){
            Property p = pIt.next();
            listener.enterProperty(p);
        }
    }
    
    protected void traverseConstructor(Constructor cons){
        listener.enterConstructor(cons);
    }
    
    protected void traverseASTNode(ASTNode node){
        listener.enterAstNode(node);

        traverseElementWithProps(node);
        
        listener.exitAstNode(node);
    }
    
    protected void traverseEntity(Entity en){
        listener.enterEntity(en);
        
        traverseElementWithProps(en);
        
        listener.exitEntity(en);
    }
    
    protected void traverseEnumerations(){
        Iterator<Enumeration> it = table.getEnumerations();
        
        listener.enterEnumerations();
        while(it.hasNext()){
            listener.enterEnumNode(it.next());
        }
        listener.exitEnumerations();
    }
    
    protected void traverseASTNodes(){
        Iterator<ASTNode> nIt = table.getNodes();
        listener.enterAstNodes();
        while(nIt.hasNext()){
            ASTNode node = nIt.next();
            traverseASTNode(node);
        }
        listener.exitAstNodes();
    }
    
    protected void traverseEntities(){
        Iterator<Entity> eIt = table.getEntities();
        listener.enterEntities();
        while(eIt.hasNext()){
            Entity en = eIt.next();
            traverseEntity(en);
        }
        listener.exitEntities();
    }
    
    public void walk(SpecListener listener){
        this.listener = listener;
        listener.enter();
        traverseEnumerations();
        traverseEntities();
        traverseASTNodes();
        listener.exit();
        this.listener = null;
    }
}
