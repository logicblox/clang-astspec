// Generated from /home/kostasferles/clang_schema/ast_spec/ParserAndGen/src/ASTSpec.g4 by ANTLR 4.1

package parser;

import errormessage.ErrorReporter;
import symboltable.SymbolTable;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ASTSpecParser}.
 */
public interface ASTSpecListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link ASTSpecParser#lang_annotations}.
	 * @param ctx the parse tree
	 */
	void enterLang_annotations(@NotNull ASTSpecParser.Lang_annotationsContext ctx);
	/**
	 * Exit a parse tree produced by {@link ASTSpecParser#lang_annotations}.
	 * @param ctx the parse tree
	 */
	void exitLang_annotations(@NotNull ASTSpecParser.Lang_annotationsContext ctx);

	/**
	 * Enter a parse tree produced by {@link ASTSpecParser#ast_element_definitions}.
	 * @param ctx the parse tree
	 */
	void enterAst_element_definitions(@NotNull ASTSpecParser.Ast_element_definitionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link ASTSpecParser#ast_element_definitions}.
	 * @param ctx the parse tree
	 */
	void exitAst_element_definitions(@NotNull ASTSpecParser.Ast_element_definitionsContext ctx);

	/**
	 * Enter a parse tree produced by {@link ASTSpecParser#property_definition}.
	 * @param ctx the parse tree
	 */
	void enterProperty_definition(@NotNull ASTSpecParser.Property_definitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ASTSpecParser#property_definition}.
	 * @param ctx the parse tree
	 */
	void exitProperty_definition(@NotNull ASTSpecParser.Property_definitionContext ctx);

	/**
	 * Enter a parse tree produced by {@link ASTSpecParser#identifier_list}.
	 * @param ctx the parse tree
	 */
	void enterIdentifier_list(@NotNull ASTSpecParser.Identifier_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link ASTSpecParser#identifier_list}.
	 * @param ctx the parse tree
	 */
	void exitIdentifier_list(@NotNull ASTSpecParser.Identifier_listContext ctx);

	/**
	 * Enter a parse tree produced by {@link ASTSpecParser#node_entity_definition}.
	 * @param ctx the parse tree
	 */
	void enterNode_entity_definition(@NotNull ASTSpecParser.Node_entity_definitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ASTSpecParser#node_entity_definition}.
	 * @param ctx the parse tree
	 */
	void exitNode_entity_definition(@NotNull ASTSpecParser.Node_entity_definitionContext ctx);

	/**
	 * Enter a parse tree produced by {@link ASTSpecParser#constructor_definition}.
	 * @param ctx the parse tree
	 */
	void enterConstructor_definition(@NotNull ASTSpecParser.Constructor_definitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ASTSpecParser#constructor_definition}.
	 * @param ctx the parse tree
	 */
	void exitConstructor_definition(@NotNull ASTSpecParser.Constructor_definitionContext ctx);

	/**
	 * Enter a parse tree produced by {@link ASTSpecParser#parameter}.
	 * @param ctx the parse tree
	 */
	void enterParameter(@NotNull ASTSpecParser.ParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link ASTSpecParser#parameter}.
	 * @param ctx the parse tree
	 */
	void exitParameter(@NotNull ASTSpecParser.ParameterContext ctx);

	/**
	 * Enter a parse tree produced by {@link ASTSpecParser#enumeration_definition}.
	 * @param ctx the parse tree
	 */
	void enterEnumeration_definition(@NotNull ASTSpecParser.Enumeration_definitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ASTSpecParser#enumeration_definition}.
	 * @param ctx the parse tree
	 */
	void exitEnumeration_definition(@NotNull ASTSpecParser.Enumeration_definitionContext ctx);

	/**
	 * Enter a parse tree produced by {@link ASTSpecParser#list_lang_annotations}.
	 * @param ctx the parse tree
	 */
	void enterList_lang_annotations(@NotNull ASTSpecParser.List_lang_annotationsContext ctx);
	/**
	 * Exit a parse tree produced by {@link ASTSpecParser#list_lang_annotations}.
	 * @param ctx the parse tree
	 */
	void exitList_lang_annotations(@NotNull ASTSpecParser.List_lang_annotationsContext ctx);

	/**
	 * Enter a parse tree produced by {@link ASTSpecParser#clang_code_for_property}.
	 * @param ctx the parse tree
	 */
	void enterClang_code_for_property(@NotNull ASTSpecParser.Clang_code_for_propertyContext ctx);
	/**
	 * Exit a parse tree produced by {@link ASTSpecParser#clang_code_for_property}.
	 * @param ctx the parse tree
	 */
	void exitClang_code_for_property(@NotNull ASTSpecParser.Clang_code_for_propertyContext ctx);

	/**
	 * Enter a parse tree produced by {@link ASTSpecParser#specification}.
	 * @param ctx the parse tree
	 */
	void enterSpecification(@NotNull ASTSpecParser.SpecificationContext ctx);
	/**
	 * Exit a parse tree produced by {@link ASTSpecParser#specification}.
	 * @param ctx the parse tree
	 */
	void exitSpecification(@NotNull ASTSpecParser.SpecificationContext ctx);

	/**
	 * Enter a parse tree produced by {@link ASTSpecParser#aux_file}.
	 * @param ctx the parse tree
	 */
	void enterAux_file(@NotNull ASTSpecParser.Aux_fileContext ctx);
	/**
	 * Exit a parse tree produced by {@link ASTSpecParser#aux_file}.
	 * @param ctx the parse tree
	 */
	void exitAux_file(@NotNull ASTSpecParser.Aux_fileContext ctx);

	/**
	 * Enter a parse tree produced by {@link ASTSpecParser#node_definition_content_element}.
	 * @param ctx the parse tree
	 */
	void enterNode_definition_content_element(@NotNull ASTSpecParser.Node_definition_content_elementContext ctx);
	/**
	 * Exit a parse tree produced by {@link ASTSpecParser#node_definition_content_element}.
	 * @param ctx the parse tree
	 */
	void exitNode_definition_content_element(@NotNull ASTSpecParser.Node_definition_content_elementContext ctx);

	/**
	 * Enter a parse tree produced by {@link ASTSpecParser#parameter_list}.
	 * @param ctx the parse tree
	 */
	void enterParameter_list(@NotNull ASTSpecParser.Parameter_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link ASTSpecParser#parameter_list}.
	 * @param ctx the parse tree
	 */
	void exitParameter_list(@NotNull ASTSpecParser.Parameter_listContext ctx);
}