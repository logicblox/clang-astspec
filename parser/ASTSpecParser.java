// Generated from /home/kostasferles/clang_schema/ast_spec/ParserAndGen/src/ASTSpec.g4 by ANTLR 4.1

package parser;

import errormessage.ErrorReporter;
import symboltable.SymbolTable;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ASTSpecParser extends Parser {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		LANG_ANNOTATIONS=1, CODE_SEGMENT=2, AST_SPEC=3, EXTENDS=4, NAME_VALUE=5, 
		PR_TYPE=6, TARGET_TYPE=7, PR_TYPE_VALUE=8, EQUALS=9, ANNOTATION_ENUMERATION=10, 
		ANNOTATION_PROPERTY=11, ANNOTATION_NODE=12, ANNOTATION_ENTITY=13, ANNOTATION_CONSTRUCTOR=14, 
		L_PAR=15, R_PAR=16, DEREF=17, CONDITION=18, ORDER=19, BOOL_LITERAL=20, 
		PROPERTY_PREFIX=21, CACHE=22, IDENTIFIER=23, PROPERTY_REFERENCE_START=24, 
		PROPERTY_REFERENCE_END=25, PROPERTY_REFERENCE=26, STRING_LITERAL=27, L_CURL=28, 
		R_CURL=29, SEMI_COLON=30, COMMA=31, L_SQBR=32, R_SQBR=33, WS=34, COMMENT=35, 
		ANY_TOKEN=36;
	public static final String[] tokenNames = {
		"<INVALID>", "LANG_ANNOTATIONS", "CODE_SEGMENT", "'ast_spec'", "'extends'", 
		"'name'", "'type'", "'targetType'", "PR_TYPE_VALUE", "'='", "'@enum'", 
		"'@property'", "'@node'", "'@entity'", "'@constructor'", "'('", "')'", 
		"'deref'", "'cond'", "'ordered'", "BOOL_LITERAL", "'prefix'", "'cache'", 
		"IDENTIFIER", "PROPERTY_REFERENCE_START", "PROPERTY_REFERENCE_END", "PROPERTY_REFERENCE", 
		"STRING_LITERAL", "'{'", "'}'", "';'", "','", "'['", "']'", "WS", "COMMENT", 
		"ANY_TOKEN"
	};
	public static final int
		RULE_specification = 0, RULE_ast_element_definitions = 1, RULE_node_entity_definition = 2, 
		RULE_node_definition_content_element = 3, RULE_constructor_definition = 4, 
		RULE_property_definition = 5, RULE_clang_code_for_property = 6, RULE_enumeration_definition = 7, 
		RULE_parameter_list = 8, RULE_parameter = 9, RULE_identifier_list = 10, 
		RULE_aux_file = 11, RULE_list_lang_annotations = 12, RULE_lang_annotations = 13;
	public static final String[] ruleNames = {
		"specification", "ast_element_definitions", "node_entity_definition", 
		"node_definition_content_element", "constructor_definition", "property_definition", 
		"clang_code_for_property", "enumeration_definition", "parameter_list", 
		"parameter", "identifier_list", "aux_file", "list_lang_annotations", "lang_annotations"
	};

	@Override
	public String getGrammarFileName() { return "ASTSpec.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public ATN getATN() { return _ATN; }



	    private int semErrors = 0;
	    
	    public void newSemError(){
	        this.semErrors++;
	    }
	    
	    @Override
	    public int getNumberOfSyntaxErrors(){
	        return this.semErrors + super.getNumberOfSyntaxErrors();
	    }
	         

	public ASTSpecParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class SpecificationContext extends ParserRuleContext {
		public TerminalNode SEMI_COLON() { return getToken(ASTSpecParser.SEMI_COLON, 0); }
		public TerminalNode EOF() { return getToken(ASTSpecParser.EOF, 0); }
		public TerminalNode IDENTIFIER() { return getToken(ASTSpecParser.IDENTIFIER, 0); }
		public TerminalNode AST_SPEC() { return getToken(ASTSpecParser.AST_SPEC, 0); }
		public Ast_element_definitionsContext ast_element_definitions() {
			return getRuleContext(Ast_element_definitionsContext.class,0);
		}
		public SpecificationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_specification; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ASTSpecListener ) ((ASTSpecListener)listener).enterSpecification(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ASTSpecListener ) ((ASTSpecListener)listener).exitSpecification(this);
		}
	}

	public final SpecificationContext specification() throws RecognitionException {
		SpecificationContext _localctx = new SpecificationContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_specification);

		      ErrorReporter reporter = ErrorReporter.getInstance();
		      reporter.setFilename(this.getSourceName());
		      reporter.setParser(this);

		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(28); match(AST_SPEC);
			setState(29); match(IDENTIFIER);
			setState(30); match(SEMI_COLON);
			setState(31); ast_element_definitions();
			setState(32); match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Ast_element_definitionsContext extends ParserRuleContext {
		public List<Node_entity_definitionContext> node_entity_definition() {
			return getRuleContexts(Node_entity_definitionContext.class);
		}
		public List<Enumeration_definitionContext> enumeration_definition() {
			return getRuleContexts(Enumeration_definitionContext.class);
		}
		public Node_entity_definitionContext node_entity_definition(int i) {
			return getRuleContext(Node_entity_definitionContext.class,i);
		}
		public Enumeration_definitionContext enumeration_definition(int i) {
			return getRuleContext(Enumeration_definitionContext.class,i);
		}
		public Ast_element_definitionsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ast_element_definitions; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ASTSpecListener ) ((ASTSpecListener)listener).enterAst_element_definitions(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ASTSpecListener ) ((ASTSpecListener)listener).exitAst_element_definitions(this);
		}
	}

	public final Ast_element_definitionsContext ast_element_definitions() throws RecognitionException {
		Ast_element_definitionsContext _localctx = new Ast_element_definitionsContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_ast_element_definitions);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(36); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				setState(36);
				switch (_input.LA(1)) {
				case ANNOTATION_NODE:
				case ANNOTATION_ENTITY:
					{
					setState(34); node_entity_definition();
					}
					break;
				case ANNOTATION_ENUMERATION:
					{
					setState(35); enumeration_definition();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(38); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ANNOTATION_ENUMERATION) | (1L << ANNOTATION_NODE) | (1L << ANNOTATION_ENTITY))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Node_entity_definitionContext extends ParserRuleContext {
		public Token thisName;
		public Token targetType;
		public Token cache;
		public Token name;
		public Token baseName;
		public TerminalNode TARGET_TYPE() { return getToken(ASTSpecParser.TARGET_TYPE, 0); }
		public TerminalNode ANNOTATION_ENTITY() { return getToken(ASTSpecParser.ANNOTATION_ENTITY, 0); }
		public TerminalNode ANNOTATION_NODE() { return getToken(ASTSpecParser.ANNOTATION_NODE, 0); }
		public List<TerminalNode> EQUALS() { return getTokens(ASTSpecParser.EQUALS); }
		public TerminalNode STRING_LITERAL() { return getToken(ASTSpecParser.STRING_LITERAL, 0); }
		public TerminalNode L_SQBR() { return getToken(ASTSpecParser.L_SQBR, 0); }
		public TerminalNode L_PAR() { return getToken(ASTSpecParser.L_PAR, 0); }
		public TerminalNode BOOL_LITERAL() { return getToken(ASTSpecParser.BOOL_LITERAL, 0); }
		public TerminalNode COMMA(int i) {
			return getToken(ASTSpecParser.COMMA, i);
		}
		public TerminalNode EQUALS(int i) {
			return getToken(ASTSpecParser.EQUALS, i);
		}
		public Node_definition_content_elementContext node_definition_content_element() {
			return getRuleContext(Node_definition_content_elementContext.class,0);
		}
		public List<TerminalNode> IDENTIFIER() { return getTokens(ASTSpecParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(ASTSpecParser.IDENTIFIER, i);
		}
		public List<TerminalNode> COMMA() { return getTokens(ASTSpecParser.COMMA); }
		public TerminalNode R_PAR() { return getToken(ASTSpecParser.R_PAR, 0); }
		public TerminalNode CACHE() { return getToken(ASTSpecParser.CACHE, 0); }
		public TerminalNode EXTENDS() { return getToken(ASTSpecParser.EXTENDS, 0); }
		public TerminalNode R_SQBR() { return getToken(ASTSpecParser.R_SQBR, 0); }
		public TerminalNode NAME_VALUE() { return getToken(ASTSpecParser.NAME_VALUE, 0); }
		public Node_entity_definitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_node_entity_definition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ASTSpecListener ) ((ASTSpecListener)listener).enterNode_entity_definition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ASTSpecListener ) ((ASTSpecListener)listener).exitNode_entity_definition(this);
		}
	}

	public final Node_entity_definitionContext node_entity_definition() throws RecognitionException {
		Node_entity_definitionContext _localctx = new Node_entity_definitionContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_node_entity_definition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(40);
			_la = _input.LA(1);
			if ( !(_la==ANNOTATION_NODE || _la==ANNOTATION_ENTITY) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			setState(41); match(L_PAR);
			setState(42); match(NAME_VALUE);
			setState(43); match(EQUALS);
			setState(44); ((Node_entity_definitionContext)_localctx).thisName = match(IDENTIFIER);
			setState(49);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				{
				setState(45); match(COMMA);
				setState(46); match(TARGET_TYPE);
				setState(47); match(EQUALS);
				setState(48); ((Node_entity_definitionContext)_localctx).targetType = match(STRING_LITERAL);
				}
				break;
			}
			setState(55);
			_la = _input.LA(1);
			if (_la==COMMA) {
				{
				setState(51); match(COMMA);
				setState(52); match(CACHE);
				setState(53); match(EQUALS);
				setState(54); ((Node_entity_definitionContext)_localctx).cache = match(BOOL_LITERAL);
				}
			}

			setState(57); match(R_PAR);
			setState(58); ((Node_entity_definitionContext)_localctx).name = match(IDENTIFIER);
			setState(61);
			_la = _input.LA(1);
			if (_la==EXTENDS) {
				{
				setState(59); match(EXTENDS);
				setState(60); ((Node_entity_definitionContext)_localctx).baseName = match(IDENTIFIER);
				}
			}

			setState(63); match(L_SQBR);
			setState(64); node_definition_content_element();
			setState(65); match(R_SQBR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Node_definition_content_elementContext extends ParserRuleContext {
		public Constructor_definitionContext constructor_definition(int i) {
			return getRuleContext(Constructor_definitionContext.class,i);
		}
		public List<Constructor_definitionContext> constructor_definition() {
			return getRuleContexts(Constructor_definitionContext.class);
		}
		public Property_definitionContext property_definition(int i) {
			return getRuleContext(Property_definitionContext.class,i);
		}
		public List<Property_definitionContext> property_definition() {
			return getRuleContexts(Property_definitionContext.class);
		}
		public Node_definition_content_elementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_node_definition_content_element; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ASTSpecListener ) ((ASTSpecListener)listener).enterNode_definition_content_element(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ASTSpecListener ) ((ASTSpecListener)listener).exitNode_definition_content_element(this);
		}
	}

	public final Node_definition_content_elementContext node_definition_content_element() throws RecognitionException {
		Node_definition_content_elementContext _localctx = new Node_definition_content_elementContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_node_definition_content_element);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(71);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ANNOTATION_PROPERTY || _la==ANNOTATION_CONSTRUCTOR) {
				{
				setState(69);
				switch (_input.LA(1)) {
				case ANNOTATION_CONSTRUCTOR:
					{
					setState(67); constructor_definition();
					}
					break;
				case ANNOTATION_PROPERTY:
					{
					setState(68); property_definition();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(73);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Constructor_definitionContext extends ParserRuleContext {
		public Identifier_listContext identifier_list() {
			return getRuleContext(Identifier_listContext.class,0);
		}
		public TerminalNode ANNOTATION_CONSTRUCTOR() { return getToken(ASTSpecParser.ANNOTATION_CONSTRUCTOR, 0); }
		public TerminalNode R_PAR() { return getToken(ASTSpecParser.R_PAR, 0); }
		public TerminalNode L_PAR() { return getToken(ASTSpecParser.L_PAR, 0); }
		public Constructor_definitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constructor_definition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ASTSpecListener ) ((ASTSpecListener)listener).enterConstructor_definition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ASTSpecListener ) ((ASTSpecListener)listener).exitConstructor_definition(this);
		}
	}

	public final Constructor_definitionContext constructor_definition() throws RecognitionException {
		Constructor_definitionContext _localctx = new Constructor_definitionContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_constructor_definition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(74); match(ANNOTATION_CONSTRUCTOR);
			setState(75); match(L_PAR);
			setState(76); identifier_list();
			setState(77); match(R_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Property_definitionContext extends ParserRuleContext {
		public Token targetType;
		public Token needsDeref;
		public Token cond;
		public Token order;
		public TerminalNode TARGET_TYPE() { return getToken(ASTSpecParser.TARGET_TYPE, 0); }
		public List<Clang_code_for_propertyContext> clang_code_for_property() {
			return getRuleContexts(Clang_code_for_propertyContext.class);
		}
		public TerminalNode STRING_LITERAL(int i) {
			return getToken(ASTSpecParser.STRING_LITERAL, i);
		}
		public TerminalNode CONDITION() { return getToken(ASTSpecParser.CONDITION, 0); }
		public List<TerminalNode> STRING_LITERAL() { return getTokens(ASTSpecParser.STRING_LITERAL); }
		public TerminalNode PR_TYPE_VALUE() { return getToken(ASTSpecParser.PR_TYPE_VALUE, 0); }
		public List<TerminalNode> L_PAR() { return getTokens(ASTSpecParser.L_PAR); }
		public Clang_code_for_propertyContext clang_code_for_property(int i) {
			return getRuleContext(Clang_code_for_propertyContext.class,i);
		}
		public TerminalNode ORDER() { return getToken(ASTSpecParser.ORDER, 0); }
		public TerminalNode IDENTIFIER() { return getToken(ASTSpecParser.IDENTIFIER, 0); }
		public TerminalNode DEREF() { return getToken(ASTSpecParser.DEREF, 0); }
		public TerminalNode R_PAR(int i) {
			return getToken(ASTSpecParser.R_PAR, i);
		}
		public TerminalNode R_SQBR() { return getToken(ASTSpecParser.R_SQBR, 0); }
		public TerminalNode BOOL_LITERAL(int i) {
			return getToken(ASTSpecParser.BOOL_LITERAL, i);
		}
		public List<TerminalNode> EQUALS() { return getTokens(ASTSpecParser.EQUALS); }
		public TerminalNode L_SQBR() { return getToken(ASTSpecParser.L_SQBR, 0); }
		public TerminalNode ANNOTATION_PROPERTY() { return getToken(ASTSpecParser.ANNOTATION_PROPERTY, 0); }
		public TerminalNode EQUALS(int i) {
			return getToken(ASTSpecParser.EQUALS, i);
		}
		public TerminalNode PR_TYPE() { return getToken(ASTSpecParser.PR_TYPE, 0); }
		public TerminalNode COMMA(int i) {
			return getToken(ASTSpecParser.COMMA, i);
		}
		public List<TerminalNode> BOOL_LITERAL() { return getTokens(ASTSpecParser.BOOL_LITERAL); }
		public List<TerminalNode> COMMA() { return getTokens(ASTSpecParser.COMMA); }
		public TerminalNode L_PAR(int i) {
			return getToken(ASTSpecParser.L_PAR, i);
		}
		public List<TerminalNode> R_PAR() { return getTokens(ASTSpecParser.R_PAR); }
		public ParameterContext parameter() {
			return getRuleContext(ParameterContext.class,0);
		}
		public Property_definitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_property_definition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ASTSpecListener ) ((ASTSpecListener)listener).enterProperty_definition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ASTSpecListener ) ((ASTSpecListener)listener).exitProperty_definition(this);
		}
	}

	public final Property_definitionContext property_definition() throws RecognitionException {
		Property_definitionContext _localctx = new Property_definitionContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_property_definition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(79); match(ANNOTATION_PROPERTY);
			setState(80); match(L_PAR);
			setState(81); match(PR_TYPE);
			setState(82); match(EQUALS);
			setState(83); match(PR_TYPE_VALUE);
			setState(88);
			switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
			case 1:
				{
				setState(84); match(COMMA);
				setState(85); match(TARGET_TYPE);
				setState(86); match(EQUALS);
				setState(87); ((Property_definitionContext)_localctx).targetType = match(STRING_LITERAL);
				}
				break;
			}
			setState(94);
			switch ( getInterpreter().adaptivePredict(_input,8,_ctx) ) {
			case 1:
				{
				setState(90); match(COMMA);
				setState(91); match(DEREF);
				setState(92); match(EQUALS);
				setState(93); ((Property_definitionContext)_localctx).needsDeref = match(BOOL_LITERAL);
				}
				break;
			}
			setState(100);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				{
				setState(96); match(COMMA);
				setState(97); match(CONDITION);
				setState(98); match(EQUALS);
				setState(99); ((Property_definitionContext)_localctx).cond = match(STRING_LITERAL);
				}
				break;
			}
			setState(106);
			_la = _input.LA(1);
			if (_la==COMMA) {
				{
				setState(102); match(COMMA);
				setState(103); match(ORDER);
				setState(104); match(EQUALS);
				setState(105); ((Property_definitionContext)_localctx).order = match(BOOL_LITERAL);
				}
			}

			setState(108); match(R_PAR);
			setState(109); match(IDENTIFIER);
			setState(110); match(L_PAR);
			setState(112);
			_la = _input.LA(1);
			if (_la==IDENTIFIER) {
				{
				setState(111); parameter();
				}
			}

			setState(114); match(R_PAR);
			setState(115); match(L_SQBR);
			setState(117); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(116); clang_code_for_property();
				}
				}
				setState(119); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << PROPERTY_REFERENCE_START) | (1L << PROPERTY_REFERENCE_END) | (1L << PROPERTY_REFERENCE) | (1L << STRING_LITERAL))) != 0) );
			setState(121); match(R_SQBR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Clang_code_for_propertyContext extends ParserRuleContext {
		public Token refStart;
		public Token refEnd;
		public Token ref;
		public TerminalNode PROPERTY_REFERENCE_START() { return getToken(ASTSpecParser.PROPERTY_REFERENCE_START, 0); }
		public TerminalNode PROPERTY_REFERENCE_END() { return getToken(ASTSpecParser.PROPERTY_REFERENCE_END, 0); }
		public TerminalNode EQUALS() { return getToken(ASTSpecParser.EQUALS, 0); }
		public TerminalNode STRING_LITERAL() { return getToken(ASTSpecParser.STRING_LITERAL, 0); }
		public TerminalNode PROPERTY_REFERENCE() { return getToken(ASTSpecParser.PROPERTY_REFERENCE, 0); }
		public Clang_code_for_propertyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_clang_code_for_property; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ASTSpecListener ) ((ASTSpecListener)listener).enterClang_code_for_property(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ASTSpecListener ) ((ASTSpecListener)listener).exitClang_code_for_property(this);
		}
	}

	public final Clang_code_for_propertyContext clang_code_for_property() throws RecognitionException {
		Clang_code_for_propertyContext _localctx = new Clang_code_for_propertyContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_clang_code_for_property);
		try {
			setState(131);
			switch (_input.LA(1)) {
			case PROPERTY_REFERENCE_START:
			case PROPERTY_REFERENCE_END:
			case PROPERTY_REFERENCE:
				enterOuterAlt(_localctx, 1);
				{
				setState(126);
				switch (_input.LA(1)) {
				case PROPERTY_REFERENCE_START:
					{
					setState(123); ((Clang_code_for_propertyContext)_localctx).refStart = match(PROPERTY_REFERENCE_START);
					}
					break;
				case PROPERTY_REFERENCE_END:
					{
					setState(124); ((Clang_code_for_propertyContext)_localctx).refEnd = match(PROPERTY_REFERENCE_END);
					}
					break;
				case PROPERTY_REFERENCE:
					{
					setState(125); ((Clang_code_for_propertyContext)_localctx).ref = match(PROPERTY_REFERENCE);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(128); match(EQUALS);
				setState(129); match(STRING_LITERAL);
				}
				break;
			case STRING_LITERAL:
				enterOuterAlt(_localctx, 2);
				{
				setState(130); match(STRING_LITERAL);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Enumeration_definitionContext extends ParserRuleContext {
		public Token prefix;
		public Token name;
		public Token IDENTIFIER;
		public List<Token> enumerator = new ArrayList<Token>();
		public TerminalNode ANNOTATION_ENUMERATION() { return getToken(ASTSpecParser.ANNOTATION_ENUMERATION, 0); }
		public TerminalNode EQUALS() { return getToken(ASTSpecParser.EQUALS, 0); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(ASTSpecParser.IDENTIFIER, i);
		}
		public List<TerminalNode> IDENTIFIER() { return getTokens(ASTSpecParser.IDENTIFIER); }
		public TerminalNode STRING_LITERAL() { return getToken(ASTSpecParser.STRING_LITERAL, 0); }
		public List<TerminalNode> COMMA() { return getTokens(ASTSpecParser.COMMA); }
		public TerminalNode PROPERTY_PREFIX() { return getToken(ASTSpecParser.PROPERTY_PREFIX, 0); }
		public TerminalNode L_SQBR() { return getToken(ASTSpecParser.L_SQBR, 0); }
		public TerminalNode R_PAR() { return getToken(ASTSpecParser.R_PAR, 0); }
		public TerminalNode L_PAR() { return getToken(ASTSpecParser.L_PAR, 0); }
		public TerminalNode COMMA(int i) {
			return getToken(ASTSpecParser.COMMA, i);
		}
		public TerminalNode R_SQBR() { return getToken(ASTSpecParser.R_SQBR, 0); }
		public Enumeration_definitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_enumeration_definition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ASTSpecListener ) ((ASTSpecListener)listener).enterEnumeration_definition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ASTSpecListener ) ((ASTSpecListener)listener).exitEnumeration_definition(this);
		}
	}

	public final Enumeration_definitionContext enumeration_definition() throws RecognitionException {
		Enumeration_definitionContext _localctx = new Enumeration_definitionContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_enumeration_definition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(133); match(ANNOTATION_ENUMERATION);
			setState(139);
			_la = _input.LA(1);
			if (_la==L_PAR) {
				{
				setState(134); match(L_PAR);
				setState(135); match(PROPERTY_PREFIX);
				setState(136); match(EQUALS);
				setState(137); ((Enumeration_definitionContext)_localctx).prefix = match(STRING_LITERAL);
				setState(138); match(R_PAR);
				}
			}

			setState(141); ((Enumeration_definitionContext)_localctx).name = match(IDENTIFIER);
			setState(142); match(L_SQBR);
			setState(143); ((Enumeration_definitionContext)_localctx).IDENTIFIER = match(IDENTIFIER);
			((Enumeration_definitionContext)_localctx).enumerator.add(((Enumeration_definitionContext)_localctx).IDENTIFIER);
			setState(148);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(144); match(COMMA);
				setState(145); ((Enumeration_definitionContext)_localctx).IDENTIFIER = match(IDENTIFIER);
				((Enumeration_definitionContext)_localctx).enumerator.add(((Enumeration_definitionContext)_localctx).IDENTIFIER);
				}
				}
				setState(150);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(151); match(R_SQBR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Parameter_listContext extends ParserRuleContext {
		public List<TerminalNode> COMMA() { return getTokens(ASTSpecParser.COMMA); }
		public List<ParameterContext> parameter() {
			return getRuleContexts(ParameterContext.class);
		}
		public ParameterContext parameter(int i) {
			return getRuleContext(ParameterContext.class,i);
		}
		public TerminalNode COMMA(int i) {
			return getToken(ASTSpecParser.COMMA, i);
		}
		public Parameter_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameter_list; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ASTSpecListener ) ((ASTSpecListener)listener).enterParameter_list(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ASTSpecListener ) ((ASTSpecListener)listener).exitParameter_list(this);
		}
	}

	public final Parameter_listContext parameter_list() throws RecognitionException {
		Parameter_listContext _localctx = new Parameter_listContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_parameter_list);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(153); parameter();
			setState(158);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(154); match(COMMA);
				setState(155); parameter();
				}
				}
				setState(160);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParameterContext extends ParserRuleContext {
		public Token type;
		public Token name;
		public TerminalNode IDENTIFIER(int i) {
			return getToken(ASTSpecParser.IDENTIFIER, i);
		}
		public List<TerminalNode> IDENTIFIER() { return getTokens(ASTSpecParser.IDENTIFIER); }
		public ParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ASTSpecListener ) ((ASTSpecListener)listener).enterParameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ASTSpecListener ) ((ASTSpecListener)listener).exitParameter(this);
		}
	}

	public final ParameterContext parameter() throws RecognitionException {
		ParameterContext _localctx = new ParameterContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_parameter);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(161); ((ParameterContext)_localctx).type = match(IDENTIFIER);
			setState(162); ((ParameterContext)_localctx).name = match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Identifier_listContext extends ParserRuleContext {
		public Token IDENTIFIER;
		public List<Token> ids = new ArrayList<Token>();
		public TerminalNode IDENTIFIER(int i) {
			return getToken(ASTSpecParser.IDENTIFIER, i);
		}
		public List<TerminalNode> IDENTIFIER() { return getTokens(ASTSpecParser.IDENTIFIER); }
		public List<TerminalNode> COMMA() { return getTokens(ASTSpecParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(ASTSpecParser.COMMA, i);
		}
		public Identifier_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_identifier_list; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ASTSpecListener ) ((ASTSpecListener)listener).enterIdentifier_list(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ASTSpecListener ) ((ASTSpecListener)listener).exitIdentifier_list(this);
		}
	}

	public final Identifier_listContext identifier_list() throws RecognitionException {
		Identifier_listContext _localctx = new Identifier_listContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_identifier_list);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(164); ((Identifier_listContext)_localctx).IDENTIFIER = match(IDENTIFIER);
			((Identifier_listContext)_localctx).ids.add(((Identifier_listContext)_localctx).IDENTIFIER);
			setState(169);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(165); match(COMMA);
				setState(166); ((Identifier_listContext)_localctx).IDENTIFIER = match(IDENTIFIER);
				((Identifier_listContext)_localctx).ids.add(((Identifier_listContext)_localctx).IDENTIFIER);
				}
				}
				setState(171);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Aux_fileContext extends ParserRuleContext {
		public List_lang_annotationsContext list_lang_annotations() {
			return getRuleContext(List_lang_annotationsContext.class,0);
		}
		public Aux_fileContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aux_file; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ASTSpecListener ) ((ASTSpecListener)listener).enterAux_file(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ASTSpecListener ) ((ASTSpecListener)listener).exitAux_file(this);
		}
	}

	public final Aux_fileContext aux_file() throws RecognitionException {
		Aux_fileContext _localctx = new Aux_fileContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_aux_file);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(172); list_lang_annotations();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class List_lang_annotationsContext extends ParserRuleContext {
		public Lang_annotationsContext lang_annotations(int i) {
			return getRuleContext(Lang_annotationsContext.class,i);
		}
		public List<Lang_annotationsContext> lang_annotations() {
			return getRuleContexts(Lang_annotationsContext.class);
		}
		public List_lang_annotationsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_list_lang_annotations; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ASTSpecListener ) ((ASTSpecListener)listener).enterList_lang_annotations(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ASTSpecListener ) ((ASTSpecListener)listener).exitList_lang_annotations(this);
		}
	}

	public final List_lang_annotationsContext list_lang_annotations() throws RecognitionException {
		List_lang_annotationsContext _localctx = new List_lang_annotationsContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_list_lang_annotations);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(175); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(174); lang_annotations();
				}
				}
				setState(177); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==LANG_ANNOTATIONS );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Lang_annotationsContext extends ParserRuleContext {
		public TerminalNode LANG_ANNOTATIONS() { return getToken(ASTSpecParser.LANG_ANNOTATIONS, 0); }
		public TerminalNode CODE_SEGMENT() { return getToken(ASTSpecParser.CODE_SEGMENT, 0); }
		public Lang_annotationsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lang_annotations; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ASTSpecListener ) ((ASTSpecListener)listener).enterLang_annotations(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ASTSpecListener ) ((ASTSpecListener)listener).exitLang_annotations(this);
		}
	}

	public final Lang_annotationsContext lang_annotations() throws RecognitionException {
		Lang_annotationsContext _localctx = new Lang_annotationsContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_lang_annotations);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(179); match(LANG_ANNOTATIONS);
			setState(180); match(CODE_SEGMENT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\uacf5\uee8c\u4f5d\u8b0d\u4a45\u78bd\u1b2f\u3378\3&\u00b9\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3"+
		"\3\6\3\'\n\3\r\3\16\3(\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4\64\n\4"+
		"\3\4\3\4\3\4\3\4\5\4:\n\4\3\4\3\4\3\4\3\4\5\4@\n\4\3\4\3\4\3\4\3\4\3\5"+
		"\3\5\7\5H\n\5\f\5\16\5K\13\5\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3"+
		"\7\3\7\3\7\3\7\5\7[\n\7\3\7\3\7\3\7\3\7\5\7a\n\7\3\7\3\7\3\7\3\7\5\7g"+
		"\n\7\3\7\3\7\3\7\3\7\5\7m\n\7\3\7\3\7\3\7\3\7\5\7s\n\7\3\7\3\7\3\7\6\7"+
		"x\n\7\r\7\16\7y\3\7\3\7\3\b\3\b\3\b\5\b\u0081\n\b\3\b\3\b\3\b\5\b\u0086"+
		"\n\b\3\t\3\t\3\t\3\t\3\t\3\t\5\t\u008e\n\t\3\t\3\t\3\t\3\t\3\t\7\t\u0095"+
		"\n\t\f\t\16\t\u0098\13\t\3\t\3\t\3\n\3\n\3\n\7\n\u009f\n\n\f\n\16\n\u00a2"+
		"\13\n\3\13\3\13\3\13\3\f\3\f\3\f\7\f\u00aa\n\f\f\f\16\f\u00ad\13\f\3\r"+
		"\3\r\3\16\6\16\u00b2\n\16\r\16\16\16\u00b3\3\17\3\17\3\17\3\17\2\20\2"+
		"\4\6\b\n\f\16\20\22\24\26\30\32\34\2\3\3\2\16\17\u00bf\2\36\3\2\2\2\4"+
		"&\3\2\2\2\6*\3\2\2\2\bI\3\2\2\2\nL\3\2\2\2\fQ\3\2\2\2\16\u0085\3\2\2\2"+
		"\20\u0087\3\2\2\2\22\u009b\3\2\2\2\24\u00a3\3\2\2\2\26\u00a6\3\2\2\2\30"+
		"\u00ae\3\2\2\2\32\u00b1\3\2\2\2\34\u00b5\3\2\2\2\36\37\7\5\2\2\37 \7\31"+
		"\2\2 !\7 \2\2!\"\5\4\3\2\"#\7\2\2\3#\3\3\2\2\2$\'\5\6\4\2%\'\5\20\t\2"+
		"&$\3\2\2\2&%\3\2\2\2\'(\3\2\2\2(&\3\2\2\2()\3\2\2\2)\5\3\2\2\2*+\t\2\2"+
		"\2+,\7\21\2\2,-\7\7\2\2-.\7\13\2\2.\63\7\31\2\2/\60\7!\2\2\60\61\7\t\2"+
		"\2\61\62\7\13\2\2\62\64\7\35\2\2\63/\3\2\2\2\63\64\3\2\2\2\649\3\2\2\2"+
		"\65\66\7!\2\2\66\67\7\30\2\2\678\7\13\2\28:\7\26\2\29\65\3\2\2\29:\3\2"+
		"\2\2:;\3\2\2\2;<\7\22\2\2<?\7\31\2\2=>\7\6\2\2>@\7\31\2\2?=\3\2\2\2?@"+
		"\3\2\2\2@A\3\2\2\2AB\7\"\2\2BC\5\b\5\2CD\7#\2\2D\7\3\2\2\2EH\5\n\6\2F"+
		"H\5\f\7\2GE\3\2\2\2GF\3\2\2\2HK\3\2\2\2IG\3\2\2\2IJ\3\2\2\2J\t\3\2\2\2"+
		"KI\3\2\2\2LM\7\20\2\2MN\7\21\2\2NO\5\26\f\2OP\7\22\2\2P\13\3\2\2\2QR\7"+
		"\r\2\2RS\7\21\2\2ST\7\b\2\2TU\7\13\2\2UZ\7\n\2\2VW\7!\2\2WX\7\t\2\2XY"+
		"\7\13\2\2Y[\7\35\2\2ZV\3\2\2\2Z[\3\2\2\2[`\3\2\2\2\\]\7!\2\2]^\7\23\2"+
		"\2^_\7\13\2\2_a\7\26\2\2`\\\3\2\2\2`a\3\2\2\2af\3\2\2\2bc\7!\2\2cd\7\24"+
		"\2\2de\7\13\2\2eg\7\35\2\2fb\3\2\2\2fg\3\2\2\2gl\3\2\2\2hi\7!\2\2ij\7"+
		"\25\2\2jk\7\13\2\2km\7\26\2\2lh\3\2\2\2lm\3\2\2\2mn\3\2\2\2no\7\22\2\2"+
		"op\7\31\2\2pr\7\21\2\2qs\5\24\13\2rq\3\2\2\2rs\3\2\2\2st\3\2\2\2tu\7\22"+
		"\2\2uw\7\"\2\2vx\5\16\b\2wv\3\2\2\2xy\3\2\2\2yw\3\2\2\2yz\3\2\2\2z{\3"+
		"\2\2\2{|\7#\2\2|\r\3\2\2\2}\u0081\7\32\2\2~\u0081\7\33\2\2\177\u0081\7"+
		"\34\2\2\u0080}\3\2\2\2\u0080~\3\2\2\2\u0080\177\3\2\2\2\u0081\u0082\3"+
		"\2\2\2\u0082\u0083\7\13\2\2\u0083\u0086\7\35\2\2\u0084\u0086\7\35\2\2"+
		"\u0085\u0080\3\2\2\2\u0085\u0084\3\2\2\2\u0086\17\3\2\2\2\u0087\u008d"+
		"\7\f\2\2\u0088\u0089\7\21\2\2\u0089\u008a\7\27\2\2\u008a\u008b\7\13\2"+
		"\2\u008b\u008c\7\35\2\2\u008c\u008e\7\22\2\2\u008d\u0088\3\2\2\2\u008d"+
		"\u008e\3\2\2\2\u008e\u008f\3\2\2\2\u008f\u0090\7\31\2\2\u0090\u0091\7"+
		"\"\2\2\u0091\u0096\7\31\2\2\u0092\u0093\7!\2\2\u0093\u0095\7\31\2\2\u0094"+
		"\u0092\3\2\2\2\u0095\u0098\3\2\2\2\u0096\u0094\3\2\2\2\u0096\u0097\3\2"+
		"\2\2\u0097\u0099\3\2\2\2\u0098\u0096\3\2\2\2\u0099\u009a\7#\2\2\u009a"+
		"\21\3\2\2\2\u009b\u00a0\5\24\13\2\u009c\u009d\7!\2\2\u009d\u009f\5\24"+
		"\13\2\u009e\u009c\3\2\2\2\u009f\u00a2\3\2\2\2\u00a0\u009e\3\2\2\2\u00a0"+
		"\u00a1\3\2\2\2\u00a1\23\3\2\2\2\u00a2\u00a0\3\2\2\2\u00a3\u00a4\7\31\2"+
		"\2\u00a4\u00a5\7\31\2\2\u00a5\25\3\2\2\2\u00a6\u00ab\7\31\2\2\u00a7\u00a8"+
		"\7!\2\2\u00a8\u00aa\7\31\2\2\u00a9\u00a7\3\2\2\2\u00aa\u00ad\3\2\2\2\u00ab"+
		"\u00a9\3\2\2\2\u00ab\u00ac\3\2\2\2\u00ac\27\3\2\2\2\u00ad\u00ab\3\2\2"+
		"\2\u00ae\u00af\5\32\16\2\u00af\31\3\2\2\2\u00b0\u00b2\5\34\17\2\u00b1"+
		"\u00b0\3\2\2\2\u00b2\u00b3\3\2\2\2\u00b3\u00b1\3\2\2\2\u00b3\u00b4\3\2"+
		"\2\2\u00b4\33\3\2\2\2\u00b5\u00b6\7\3\2\2\u00b6\u00b7\7\4\2\2\u00b7\35"+
		"\3\2\2\2\26&(\639?GIZ`flry\u0080\u0085\u008d\u0096\u00a0\u00ab\u00b3";
	public static final ATN _ATN =
		ATNSimulator.deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}