// Generated from /home/kostasferles/clang_schema/ast_spec/ParserAndGen/src/ASTSpec.g4 by ANTLR 4.1

package parser;

import errormessage.ErrorReporter;
import symboltable.SymbolTable;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ASTSpecLexer extends Lexer {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		LANG_ANNOTATIONS=1, CODE_SEGMENT=2, AST_SPEC=3, EXTENDS=4, NAME_VALUE=5, 
		PR_TYPE=6, TARGET_TYPE=7, PR_TYPE_VALUE=8, EQUALS=9, ANNOTATION_ENUMERATION=10, 
		ANNOTATION_PROPERTY=11, ANNOTATION_NODE=12, ANNOTATION_ENTITY=13, ANNOTATION_CONSTRUCTOR=14, 
		L_PAR=15, R_PAR=16, DEREF=17, CONDITION=18, ORDER=19, BOOL_LITERAL=20, 
		PROPERTY_PREFIX=21, CACHE=22, IDENTIFIER=23, PROPERTY_REFERENCE_START=24, 
		PROPERTY_REFERENCE_END=25, PROPERTY_REFERENCE=26, STRING_LITERAL=27, L_CURL=28, 
		R_CURL=29, SEMI_COLON=30, COMMA=31, L_SQBR=32, R_SQBR=33, WS=34, COMMENT=35, 
		ANY_TOKEN=36;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] tokenNames = {
		"<INVALID>",
		"LANG_ANNOTATIONS", "CODE_SEGMENT", "'ast_spec'", "'extends'", "'name'", 
		"'type'", "'targetType'", "PR_TYPE_VALUE", "'='", "'@enum'", "'@property'", 
		"'@node'", "'@entity'", "'@constructor'", "'('", "')'", "'deref'", "'cond'", 
		"'ordered'", "BOOL_LITERAL", "'prefix'", "'cache'", "IDENTIFIER", "PROPERTY_REFERENCE_START", 
		"PROPERTY_REFERENCE_END", "PROPERTY_REFERENCE", "STRING_LITERAL", "'{'", 
		"'}'", "';'", "','", "'['", "']'", "WS", "COMMENT", "ANY_TOKEN"
	};
	public static final String[] ruleNames = {
		"LANG_ANNOTATIONS", "CODE_SEGMENT", "AST_SPEC", "EXTENDS", "NAME_VALUE", 
		"PR_TYPE", "TARGET_TYPE", "PR_TYPE_VALUE", "EQUALS", "ANNOTATION_ENUMERATION", 
		"ANNOTATION_PROPERTY", "ANNOTATION_NODE", "ANNOTATION_ENTITY", "ANNOTATION_CONSTRUCTOR", 
		"L_PAR", "R_PAR", "DEREF", "CONDITION", "ORDER", "BOOL_LITERAL", "PROPERTY_PREFIX", 
		"CACHE", "IDENTIFIER", "PROPERTY_REFERENCE_START", "PROPERTY_REFERENCE_END", 
		"PROPERTY_REFERENCE", "STRING_LITERAL", "L_CURL", "R_CURL", "SEMI_COLON", 
		"COMMA", "L_SQBR", "R_SQBR", "WS", "COMMENT", "ANY_TOKEN"
	};


	public ASTSpecLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "ASTSpec.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	@Override
	public void action(RuleContext _localctx, int ruleIndex, int actionIndex) {
		switch (ruleIndex) {
		case 33: WS_action((RuleContext)_localctx, actionIndex); break;

		case 34: COMMENT_action((RuleContext)_localctx, actionIndex); break;
		}
	}
	private void WS_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 0: skip();  break;
		}
	}
	private void COMMENT_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 1: skip();  break;
		}
	}

	public static final String _serializedATN =
		"\3\uacf5\uee8c\u4f5d\u8b0d\u4a45\u78bd\u1b2f\u3378\2&\u0189\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2"+
		"\3\2\3\2\3\2\3\2\3\2\5\2[\n\2\3\3\3\3\3\3\7\3`\n\3\f\3\16\3c\13\3\3\3"+
		"\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3"+
		"\5\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b"+
		"\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3"+
		"\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t"+
		"\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3"+
		"\t\3\t\3\t\3\t\3\t\3\t\3\t\5\t\u00c4\n\t\3\n\3\n\3\13\3\13\3\13\3\13\3"+
		"\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3"+
		"\r\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3"+
		"\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\20\3\20\3\21\3\21\3\22\3\22\3"+
		"\22\3\22\3\22\3\22\3\23\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\24\3\24\3"+
		"\24\3\24\3\24\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\5\25\u0113"+
		"\n\25\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\27\3\27\3\27\3\27\3\27\3\27"+
		"\3\30\3\30\7\30\u0124\n\30\f\30\16\30\u0127\13\30\3\31\3\31\3\31\7\31"+
		"\u012c\n\31\f\31\16\31\u012f\13\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31"+
		"\3\32\3\32\3\32\7\32\u013b\n\32\f\32\16\32\u013e\13\32\3\32\3\32\3\32"+
		"\3\32\3\32\3\33\3\33\3\33\7\33\u0148\n\33\f\33\16\33\u014b\13\33\3\34"+
		"\3\34\7\34\u014f\n\34\f\34\16\34\u0152\13\34\3\34\3\34\3\35\3\35\3\36"+
		"\3\36\3\37\3\37\3 \3 \3!\3!\3\"\3\"\3#\6#\u0163\n#\r#\16#\u0164\3#\3#"+
		"\3$\3$\3$\3$\7$\u016d\n$\f$\16$\u0170\13$\3$\3$\3$\3$\3$\3$\7$\u0178\n"+
		"$\f$\16$\u017b\13$\3$\5$\u017e\n$\3$\3$\5$\u0182\n$\5$\u0184\n$\3$\3$"+
		"\3%\3%\5\u0150\u016e\u0179&\3\3\1\5\4\1\7\5\1\t\6\1\13\7\1\r\b\1\17\t"+
		"\1\21\n\1\23\13\1\25\f\1\27\r\1\31\16\1\33\17\1\35\20\1\37\21\1!\22\1"+
		"#\23\1%\24\1\'\25\1)\26\1+\27\1-\30\1/\31\1\61\32\1\63\33\1\65\34\1\67"+
		"\35\19\36\1;\37\1= \1?!\1A\"\1C#\1E$\2G%\3I&\1\3\2\6\4\2}}\177\177\5\2"+
		"C\\aac|\6\2\62;C\\aac|\5\2\13\f\17\17\"\"\u019b\2\3\3\2\2\2\2\5\3\2\2"+
		"\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21"+
		"\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2"+
		"\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3"+
		"\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3"+
		"\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3"+
		"\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\3Z\3\2\2"+
		"\2\5\\\3\2\2\2\7f\3\2\2\2\to\3\2\2\2\13w\3\2\2\2\r|\3\2\2\2\17\u0081\3"+
		"\2\2\2\21\u00c3\3\2\2\2\23\u00c5\3\2\2\2\25\u00c7\3\2\2\2\27\u00cd\3\2"+
		"\2\2\31\u00d7\3\2\2\2\33\u00dd\3\2\2\2\35\u00e5\3\2\2\2\37\u00f2\3\2\2"+
		"\2!\u00f4\3\2\2\2#\u00f6\3\2\2\2%\u00fc\3\2\2\2\'\u0101\3\2\2\2)\u0112"+
		"\3\2\2\2+\u0114\3\2\2\2-\u011b\3\2\2\2/\u0121\3\2\2\2\61\u0128\3\2\2\2"+
		"\63\u0137\3\2\2\2\65\u0144\3\2\2\2\67\u014c\3\2\2\29\u0155\3\2\2\2;\u0157"+
		"\3\2\2\2=\u0159\3\2\2\2?\u015b\3\2\2\2A\u015d\3\2\2\2C\u015f\3\2\2\2E"+
		"\u0162\3\2\2\2G\u0183\3\2\2\2I\u0187\3\2\2\2KL\7B\2\2LM\7j\2\2MN\7g\2"+
		"\2NO\7c\2\2OP\7f\2\2PQ\7g\2\2Q[\7t\2\2RS\7B\2\2ST\7o\2\2TU\7g\2\2UV\7"+
		"o\2\2VW\7d\2\2WX\7g\2\2XY\7t\2\2Y[\7u\2\2ZK\3\2\2\2ZR\3\2\2\2[\4\3\2\2"+
		"\2\\a\7}\2\2]`\5\5\3\2^`\n\2\2\2_]\3\2\2\2_^\3\2\2\2`c\3\2\2\2a_\3\2\2"+
		"\2ab\3\2\2\2bd\3\2\2\2ca\3\2\2\2de\7\177\2\2e\6\3\2\2\2fg\7c\2\2gh\7u"+
		"\2\2hi\7v\2\2ij\7a\2\2jk\7u\2\2kl\7r\2\2lm\7g\2\2mn\7e\2\2n\b\3\2\2\2"+
		"op\7g\2\2pq\7z\2\2qr\7v\2\2rs\7g\2\2st\7p\2\2tu\7f\2\2uv\7u\2\2v\n\3\2"+
		"\2\2wx\7p\2\2xy\7c\2\2yz\7o\2\2z{\7g\2\2{\f\3\2\2\2|}\7v\2\2}~\7{\2\2"+
		"~\177\7r\2\2\177\u0080\7g\2\2\u0080\16\3\2\2\2\u0081\u0082\7v\2\2\u0082"+
		"\u0083\7c\2\2\u0083\u0084\7t\2\2\u0084\u0085\7i\2\2\u0085\u0086\7g\2\2"+
		"\u0086\u0087\7v\2\2\u0087\u0088\7V\2\2\u0088\u0089\7{\2\2\u0089\u008a"+
		"\7r\2\2\u008a\u008b\7g\2\2\u008b\20\3\2\2\2\u008c\u008d\7o\2\2\u008d\u008e"+
		"\7c\2\2\u008e\u008f\7p\2\2\u008f\u0090\7{\2\2\u0090\u0091\7/\2\2\u0091"+
		"\u0092\7v\2\2\u0092\u0093\7q\2\2\u0093\u0094\7/\2\2\u0094\u0095\7q\2\2"+
		"\u0095\u0096\7p\2\2\u0096\u00c4\7g\2\2\u0097\u0098\7q\2\2\u0098\u0099"+
		"\7p\2\2\u0099\u009a\7g\2\2\u009a\u009b\7/\2\2\u009b\u009c\7v\2\2\u009c"+
		"\u009d\7q\2\2\u009d\u009e\7/\2\2\u009e\u009f\7o\2\2\u009f\u00a0\7c\2\2"+
		"\u00a0\u00a1\7p\2\2\u00a1\u00c4\7{\2\2\u00a2\u00a3\7q\2\2\u00a3\u00a4"+
		"\7p\2\2\u00a4\u00a5\7g\2\2\u00a5\u00a6\7/\2\2\u00a6\u00a7\7v\2\2\u00a7"+
		"\u00a8\7q\2\2\u00a8\u00a9\7/\2\2\u00a9\u00aa\7q\2\2\u00aa\u00ab\7p\2\2"+
		"\u00ab\u00c4\7g\2\2\u00ac\u00ad\7o\2\2\u00ad\u00ae\7c\2\2\u00ae\u00af"+
		"\7p\2\2\u00af\u00b0\7{\2\2\u00b0\u00b1\7/\2\2\u00b1\u00b2\7v\2\2\u00b2"+
		"\u00b3\7q\2\2\u00b3\u00b4\7/\2\2\u00b4\u00b5\7o\2\2\u00b5\u00b6\7c\2\2"+
		"\u00b6\u00b7\7p\2\2\u00b7\u00c4\7{\2\2\u00b8\u00b9\7g\2\2\u00b9\u00ba"+
		"\7z\2\2\u00ba\u00bb\7k\2\2\u00bb\u00bc\7u\2\2\u00bc\u00bd\7v\2\2\u00bd"+
		"\u00be\7g\2\2\u00be\u00bf\7p\2\2\u00bf\u00c0\7v\2\2\u00c0\u00c1\7k\2\2"+
		"\u00c1\u00c2\7c\2\2\u00c2\u00c4\7n\2\2\u00c3\u008c\3\2\2\2\u00c3\u0097"+
		"\3\2\2\2\u00c3\u00a2\3\2\2\2\u00c3\u00ac\3\2\2\2\u00c3\u00b8\3\2\2\2\u00c4"+
		"\22\3\2\2\2\u00c5\u00c6\7?\2\2\u00c6\24\3\2\2\2\u00c7\u00c8\7B\2\2\u00c8"+
		"\u00c9\7g\2\2\u00c9\u00ca\7p\2\2\u00ca\u00cb\7w\2\2\u00cb\u00cc\7o\2\2"+
		"\u00cc\26\3\2\2\2\u00cd\u00ce\7B\2\2\u00ce\u00cf\7r\2\2\u00cf\u00d0\7"+
		"t\2\2\u00d0\u00d1\7q\2\2\u00d1\u00d2\7r\2\2\u00d2\u00d3\7g\2\2\u00d3\u00d4"+
		"\7t\2\2\u00d4\u00d5\7v\2\2\u00d5\u00d6\7{\2\2\u00d6\30\3\2\2\2\u00d7\u00d8"+
		"\7B\2\2\u00d8\u00d9\7p\2\2\u00d9\u00da\7q\2\2\u00da\u00db\7f\2\2\u00db"+
		"\u00dc\7g\2\2\u00dc\32\3\2\2\2\u00dd\u00de\7B\2\2\u00de\u00df\7g\2\2\u00df"+
		"\u00e0\7p\2\2\u00e0\u00e1\7v\2\2\u00e1\u00e2\7k\2\2\u00e2\u00e3\7v\2\2"+
		"\u00e3\u00e4\7{\2\2\u00e4\34\3\2\2\2\u00e5\u00e6\7B\2\2\u00e6\u00e7\7"+
		"e\2\2\u00e7\u00e8\7q\2\2\u00e8\u00e9\7p\2\2\u00e9\u00ea\7u\2\2\u00ea\u00eb"+
		"\7v\2\2\u00eb\u00ec\7t\2\2\u00ec\u00ed\7w\2\2\u00ed\u00ee\7e\2\2\u00ee"+
		"\u00ef\7v\2\2\u00ef\u00f0\7q\2\2\u00f0\u00f1\7t\2\2\u00f1\36\3\2\2\2\u00f2"+
		"\u00f3\7*\2\2\u00f3 \3\2\2\2\u00f4\u00f5\7+\2\2\u00f5\"\3\2\2\2\u00f6"+
		"\u00f7\7f\2\2\u00f7\u00f8\7g\2\2\u00f8\u00f9\7t\2\2\u00f9\u00fa\7g\2\2"+
		"\u00fa\u00fb\7h\2\2\u00fb$\3\2\2\2\u00fc\u00fd\7e\2\2\u00fd\u00fe\7q\2"+
		"\2\u00fe\u00ff\7p\2\2\u00ff\u0100\7f\2\2\u0100&\3\2\2\2\u0101\u0102\7"+
		"q\2\2\u0102\u0103\7t\2\2\u0103\u0104\7f\2\2\u0104\u0105\7g\2\2\u0105\u0106"+
		"\7t\2\2\u0106\u0107\7g\2\2\u0107\u0108\7f\2\2\u0108(\3\2\2\2\u0109\u010a"+
		"\7v\2\2\u010a\u010b\7t\2\2\u010b\u010c\7w\2\2\u010c\u0113\7g\2\2\u010d"+
		"\u010e\7h\2\2\u010e\u010f\7c\2\2\u010f\u0110\7n\2\2\u0110\u0111\7u\2\2"+
		"\u0111\u0113\7g\2\2\u0112\u0109\3\2\2\2\u0112\u010d\3\2\2\2\u0113*\3\2"+
		"\2\2\u0114\u0115\7r\2\2\u0115\u0116\7t\2\2\u0116\u0117\7g\2\2\u0117\u0118"+
		"\7h\2\2\u0118\u0119\7k\2\2\u0119\u011a\7z\2\2\u011a,\3\2\2\2\u011b\u011c"+
		"\7e\2\2\u011c\u011d\7c\2\2\u011d\u011e\7e\2\2\u011e\u011f\7j\2\2\u011f"+
		"\u0120\7g\2\2\u0120.\3\2\2\2\u0121\u0125\t\3\2\2\u0122\u0124\t\4\2\2\u0123"+
		"\u0122\3\2\2\2\u0124\u0127\3\2\2\2\u0125\u0123\3\2\2\2\u0125\u0126\3\2"+
		"\2\2\u0126\60\3\2\2\2\u0127\u0125\3\2\2\2\u0128\u0129\7&\2\2\u0129\u012d"+
		"\t\3\2\2\u012a\u012c\t\4\2\2\u012b\u012a\3\2\2\2\u012c\u012f\3\2\2\2\u012d"+
		"\u012b\3\2\2\2\u012d\u012e\3\2\2\2\u012e\u0130\3\2\2\2\u012f\u012d\3\2"+
		"\2\2\u0130\u0131\7<\2\2\u0131\u0132\7u\2\2\u0132\u0133\7v\2\2\u0133\u0134"+
		"\7c\2\2\u0134\u0135\7t\2\2\u0135\u0136\7v\2\2\u0136\62\3\2\2\2\u0137\u0138"+
		"\7&\2\2\u0138\u013c\t\3\2\2\u0139\u013b\t\4\2\2\u013a\u0139\3\2\2\2\u013b"+
		"\u013e\3\2\2\2\u013c\u013a\3\2\2\2\u013c\u013d\3\2\2\2\u013d\u013f\3\2"+
		"\2\2\u013e\u013c\3\2\2\2\u013f\u0140\7<\2\2\u0140\u0141\7g\2\2\u0141\u0142"+
		"\7p\2\2\u0142\u0143\7f\2\2\u0143\64\3\2\2\2\u0144\u0145\7&\2\2\u0145\u0149"+
		"\t\3\2\2\u0146\u0148\t\4\2\2\u0147\u0146\3\2\2\2\u0148\u014b\3\2\2\2\u0149"+
		"\u0147\3\2\2\2\u0149\u014a\3\2\2\2\u014a\66\3\2\2\2\u014b\u0149\3\2\2"+
		"\2\u014c\u0150\7$\2\2\u014d\u014f\5I%\2\u014e\u014d\3\2\2\2\u014f\u0152"+
		"\3\2\2\2\u0150\u0151\3\2\2\2\u0150\u014e\3\2\2\2\u0151\u0153\3\2\2\2\u0152"+
		"\u0150\3\2\2\2\u0153\u0154\7$\2\2\u01548\3\2\2\2\u0155\u0156\7}\2\2\u0156"+
		":\3\2\2\2\u0157\u0158\7\177\2\2\u0158<\3\2\2\2\u0159\u015a\7=\2\2\u015a"+
		">\3\2\2\2\u015b\u015c\7.\2\2\u015c@\3\2\2\2\u015d\u015e\7]\2\2\u015eB"+
		"\3\2\2\2\u015f\u0160\7_\2\2\u0160D\3\2\2\2\u0161\u0163\t\5\2\2\u0162\u0161"+
		"\3\2\2\2\u0163\u0164\3\2\2\2\u0164\u0162\3\2\2\2\u0164\u0165\3\2\2\2\u0165"+
		"\u0166\3\2\2\2\u0166\u0167\b#\2\2\u0167F\3\2\2\2\u0168\u0169\7\61\2\2"+
		"\u0169\u016a\7,\2\2\u016a\u016e\3\2\2\2\u016b\u016d\13\2\2\2\u016c\u016b"+
		"\3\2\2\2\u016d\u0170\3\2\2\2\u016e\u016f\3\2\2\2\u016e\u016c\3\2\2\2\u016f"+
		"\u0171\3\2\2\2\u0170\u016e\3\2\2\2\u0171\u0172\7,\2\2\u0172\u0184\7\61"+
		"\2\2\u0173\u0174\7\61\2\2\u0174\u0175\7\61\2\2\u0175\u0179\3\2\2\2\u0176"+
		"\u0178\13\2\2\2\u0177\u0176\3\2\2\2\u0178\u017b\3\2\2\2\u0179\u017a\3"+
		"\2\2\2\u0179\u0177\3\2\2\2\u017a\u0181\3\2\2\2\u017b\u0179\3\2\2\2\u017c"+
		"\u017e\7\17\2\2\u017d\u017c\3\2\2\2\u017d\u017e\3\2\2\2\u017e\u017f\3"+
		"\2\2\2\u017f\u0182\7\f\2\2\u0180\u0182\7\2\2\3\u0181\u017d\3\2\2\2\u0181"+
		"\u0180\3\2\2\2\u0182\u0184\3\2\2\2\u0183\u0168\3\2\2\2\u0183\u0173\3\2"+
		"\2\2\u0184\u0185\3\2\2\2\u0185\u0186\b$\3\2\u0186H\3\2\2\2\u0187\u0188"+
		"\13\2\2\2\u0188J\3\2\2\2\23\2Z_a\u00c3\u0112\u0125\u012d\u013c\u0149\u0150"+
		"\u0164\u016e\u0179\u017d\u0181\u0183";
	public static final ATN _ATN =
		ATNSimulator.deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}