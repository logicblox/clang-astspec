package clangAST;

import java.io.IOException;
import listener.GatherAuxCppCodeSegments;
import listener.GatherProperties;
import listener.GatherTopLevelDeclarations;
import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import parser.ASTSpecLexer;
import parser.ASTSpecParser;

/**
 *
 * @author kostasferles
 */
public class Driver {
    
    private static ASTSpecParser getParser(String filename) throws IOException{
        ASTSpecLexer lexer = new ASTSpecLexer(new ANTLRFileStream(filename));
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        return new ASTSpecParser(tokens);
    }
    
    private static void parseSpecFile(ASTSpecParser parser){
        ParseTree tree = parser.specification();
        if(parser.getNumberOfSyntaxErrors() == 0){
            ParseTreeWalker walker = new ParseTreeWalker();
            GatherTopLevelDeclarations gatherDecls = new GatherTopLevelDeclarations();
            GatherProperties gatherProperties = new GatherProperties();
            walker.walk(gatherDecls, tree);
            walker.walk(gatherProperties, tree);
        }
    }
    
    private static void parseAuxFile(ASTSpecParser parser){
        ParseTree tree = parser.aux_file();
        if(parser.getNumberOfSyntaxErrors() == 0){
            ParseTreeWalker walker = new ParseTreeWalker();
            GatherAuxCppCodeSegments segs = new GatherAuxCppCodeSegments();
            walker.walk(segs, tree);
        }
    }

    public static void main(String[] args){
        if(args.length > 2){
            System.err.println("Usage: java Driver <input_file_spec> [<aux_file>]");
            System.exit(1);
        }
        
        if(args.length == 1){
            try{
                ASTSpecParser parser = getParser(args[0]);
                parseSpecFile(parser);
            }
            catch(IOException ex){
                System.err.println(ex.getMessage());
                System.exit(1);
            }
        }
        
        if(args.length == 2){
            try{
                ASTSpecParser specParser = getParser(args[0]);
                parseSpecFile(specParser);
                
                ASTSpecParser auxParser = getParser(args[1]);
                parseAuxFile(auxParser);
            }
            catch(IOException ex){
                System.err.println(ex.getMessage());
                System.exit(1);
            }
        }
        System.exit(0);
    }
    
    public static int parseSpecFile(String specFile) throws IOException{
        ASTSpecParser specParser = getParser(specFile);
        parseSpecFile(specParser);
        return specParser.getNumberOfSyntaxErrors();
    }
    
    public static int parseAuxFile(String auxInputFile) throws IOException {
        ASTSpecParser auxParser = getParser(auxInputFile);
        parseAuxFile(auxParser);
        return auxParser.getNumberOfSyntaxErrors();
    }
    
    public static int parseSpecAndAuxFile(String specFile, String auxFile) throws IOException{
        ASTSpecParser specParser = getParser(specFile);
        parseSpecFile(specParser);
        ASTSpecParser auxParser = getParser(auxFile);
        parseAuxFile(auxParser);
        
        return specParser.getNumberOfSyntaxErrors() + auxParser.getNumberOfSyntaxErrors();
    }
    
}
