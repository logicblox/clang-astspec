ast_spec ClangAst;

@enum
TagTypeKind[
  TTK_Struct,
  TTK_Interface,
  TTK_Union,
  TTK_Class,
  TTK_Enum
]

@enum
AccessSpecifier[
  AS_public,
  AS_protected,
  AS_private,
  AS_none
]

@enum(prefix = "ArrayType")
ArraySizeModifier[
  Normal,
  Static,
  Star
]

@enum(prefix = "SrcMgr")
CharacteristicKind[
  C_User,
  C_System,
  C_ExternCSystem
]

@enum(prefix = "VarDecl")
DefinitionKind[
  DeclarationOnly,
  TentativeDefinition,
  Definition
]

@enum
CallingConv[
  CC_C,	
  CC_X86StdCall,
  CC_X86FastCall,
  CC_X86ThisCall,
  CC_X86Pascal,
  CC_X86_64Win64,
  CC_X86_64SysV,
  CC_X86VectorCall,
  CC_AAPCS,
  CC_AAPCS_VFP,
  CC_IntelOclBicc,
  CC_SpirFunction,
  CC_SpirKernel
/*  CC_OpenCLKernel,
  CC_Swift,
  CC_PreserveMost,
  CC_PreserveAll */
]

@enum
BinaryOperatorKind[
  BO_PtrMemD, BO_PtrMemI,
  BO_Mul, BO_Div, BO_Rem, BO_Add, BO_Sub,
  BO_Shl, BO_Shr,
  BO_LT, BO_GT, BO_LE, BO_GE, BO_EQ, BO_NE,
  BO_And, BO_Xor, BO_Or, BO_LAnd, BO_LOr,
  BO_Assign, BO_MulAssign, BO_DivAssign, BO_RemAssign, BO_AddAssign,
  BO_SubAssign, BO_ShlAssign, BO_ShrAssign, BO_AndAssign, BO_XorAssign, BO_OrAssign,
  BO_Comma
]

@enum(prefix = "CharacterLiteral")
CharacterKind[
  Ascii, Wide, UTF8, UTF16, UTF32
]

@enum
UnaryOperatorKind[
  UO_PostInc, UO_PostDec, UO_PreInc, UO_PreDec,
  UO_AddrOf, UO_Deref,
  UO_Plus, UO_Minus, UO_Not, UO_LNot,
  UO_Real, UO_Imag, UO_Extension, UO_Coawait
]

@enum
UnaryExprOrTypeTrait[
  UETT_SizeOf, UETT_AlignOf, UETT_VecStep, UETT_OpenMPRequiredSimdAlign
]

@enum(prefix = "CXXConstructExpr")
ConstructionKind[
  CK_Complete,
  CK_NonVirtualBase,
  CK_VirtualBase,
  CK_Delegating
]

@enum(prefix = "CXXNewExpr")
InitializationStyle[
  NoInit, CallInit, ListInit
]


@node(name = D,
      targetType = "const Decl *")
Decl[

  @constructor(uid)

  @property(type = many-to-one)
  uid(int i)[
    $i = "getUID( D )"
  ]

  @property(type = many-to-one,
            cond = "getSourceFile(SrcManager.getPresumedLoc(D->getLocStart(), false)) != NULL")
  sourceFile(FileEntry sourcefile)[
    $sourcefile = "getSourceFile(SrcManager.getPresumedLoc(D->getLocStart(), false))"
  ]

  @property(type = many-to-one)
  sourceLocationStart(Location locationStart)[
    $locationStart = "getSourceLocation(Location::BEGIN, D)"
  ]

  @property(type = many-to-one)
  sourceLocationEnd(Location locationEnd)[
    $locationEnd = "getSourceLocation(Location::END, D)"
  ]

  @property(type = many-to-one)
  accessSpecifier(AccessSpecifier as)[
    $as = "D->getAccess()"
  ]

]

@node(name = D, targetType = "AccessSpecDecl *")
AccessSpecDecl extends Decl[ ]

@node(name = D, targetType = "FriendDecl *")
FriendDecl extends Decl[

/*
 * Friend class
 */
  @property(type = many-to-many,
            cond = "D->getFriendType() != NULL")
  getType(QualType t)[
    $t = "D->getFriendType()->getType()"
  ]

/*
 * Friend function
 */
  @property(type = many-to-many,
            cond = "D->getFriendDecl() != NULL")
  getDecl(NamedDecl d)[
    $d = "D->getFriendDecl()"
  ]

]

@node(name = D,
      targetType = "LinkageSpecDecl *")
LinkageSpecDecl extends Decl[

  @property(type = many-to-one)
  language(string l)[
    $l = "getLanguage( D )"
  ]

]

@node(name = D,
      targetType = "NamedDecl *")
NamedDecl extends Decl[

  @property(type = many-to-one)
  qName(string n)[
    $n = "getQName( D )"
  ]

  @property(type = many-to-one)
  sName(string n)[
    $n = "getName( D )"
  ]

]

@node(name = D, targetType = "LabelDecl *")
LabelDecl extends NamedDecl[ ]

@node(name = D,
      targetType = "TypeDecl *")
TypeDecl extends NamedDecl[

  @property(type = many-to-one,
            cond = "D->getTypeForDecl() != NULL")
  hasType(Type t)[
    $t = "D->getTypeForDecl()"
  ]

]

@node(name = D,
      targetType = "NamespaceDecl *")
NamespaceDecl extends NamedDecl[

  @property(type = existential)
  isAnonymous()[
    "D->isAnonymousNamespace()"
  ]

  @property(type = existential)
  isInline()[
    "D->isInline()"
  ]

  /* If we need this property
   * it is not many-to-one
   * the original namespace
   * depends on the include
   * hierarchy 
  @property(type = many-to-one,
            cond = "D != D->getOriginalNamespace()")
  hasOriginal(NamespaceDecl orig)[
    $orig = "D->getOriginalNamespace()"
  ]
  */

]

@node(name = D,
      targetType = "NamespaceAliasDecl *")
NamespaceAliasDecl extends NamedDecl[

  @property(type = many-to-one)
  aliasedNamespace(NamedDecl nd)[
    $nd = "D->getAliasedNamespace()"
  ]

]

@node(name = D,
      targetType = "UsingShadowDecl *")
UsingShadowDecl extends NamedDecl[

  @property(type = many-to-one)
  targetDecl(NamedDecl nd)[
    $nd = "D->getTargetDecl()"
  ]

]

@node(name = D,
      targetType = "UsingDecl *")
UsingDecl extends NamedDecl[

  @property(type = many-to-one)
  hasNestedNameSpecifier(NestedNameSpecifier ns)[
    $ns = "D->getQualifier()"
  ]

  @property(type = many-to-many,
  	        targetType = "UsingDecl::shadow_iterator",
            deref = true)
  hasShadowDecl(UsingShadowDecl shadow)[
    $shadow:start = "D->shadow_begin()"
    $shadow:end = "D->shadow_end()"
  ]

]

@node(name = D,
      targetType = "UsingDirectiveDecl *")
UsingDirectiveDecl extends NamedDecl[

  @property(type = many-to-one)
  namespace(NamedDecl ns)[
    $ns = "D->getNominatedNamespaceAsWritten()"
  ]

]

@node(name = D,
      targetType = "ValueDecl *")
ValueDecl extends NamedDecl[

  @property(type = many-to-one)
  hasType(QualType qType)[
    $qType = "D->getType()"
  ]

]

/*
 * Represents a dependent using declaration
 * template <typename T> class A : public Base<T> {
 *   using Base<T>::foo;
 * };
 */
@node(name = D, targetType = "UnresolvedUsingValueDecl *")
UnresolvedUsingValueDecl extends ValueDecl[

  @property(type = many-to-one)
  hasNestedNameSpecifier(NestedNameSpecifier ns)[
    $ns = "D->getQualifier()"
  ]

]

@node(name = D, targetType = "DeclaratorDecl *")
DeclaratorDecl extends ValueDecl[ ]

@node(name = D,
      targetType = "FieldDecl *")
FieldDecl extends DeclaratorDecl[

  @property(type = many-to-one)
  index(int i)[
    $i = "D->getFieldIndex()"
  ]

  @property(type = existential)
  isBitField()[
    "D->isBitField()"
  ]

  @property(type = many-to-one)
  declaringRecord(RecordDecl r)[
    $r = "D->getParent()"
  ]

]

@node(name = D, targetType = "FunctionDecl *")
FunctionDecl extends DeclaratorDecl[

  @property(type = existential)
  isDefinition()[
    "D->getBody() != NULL"
  ]

  @property(type = existential)
  isVariadic()[
    "D->isVariadic()"
  ]

  @property(type = existential)
  isExplicitVirtual()[
    "D->isVirtualAsWritten()"
  ]

  @property(type = existential)
  isPure()[
    "D->isPure()"
  ]

  @property(type = existential)
  isMain()[
    "D->isMain()"
  ]

  @property(type = existential)
  isExternCFunction()[
    "D->isExternC()"
  ]

  @property(type = existential)
  isGlobal()[
    "D->isGlobal()"
  ]

  @property(type = existential)
  isExtern()[
    "D->getStorageClass() == SC_Extern"
  ]

  @property(type = existential)
  isStatic()[
    "D->getStorageClass() == SC_Static"
  ]

/*
 * E.g. in "int & foo ();"
 * the resultType is "int &"
 * while the resultTypeForCall is "int"
 */
  @property(type = many-to-one)
  resultType(QualType t)[
    $t = "D->getReturnType()"
  ]

  @property(type = many-to-one)
  resultTypeForCall(QualType t)[
    $t = "D->getCallResultType()"
  ]

  @property(type = existential)
  isInlined()[
    "D->isInlined()"
  ]

  @property(type = existential)
  isOverloadedOperator()[
    "D->isOverloadedOperator()"
  ]

  @property(type = many-to-many,
  	        targetType = "FunctionDecl::param_iterator",
            deref = true)
  hasParameter(ParmVarDecl param)[
    $param:start = "D->param_begin()"
    $param:end = "D->param_end()"
  ]

  @property(type = many-to-one,
            cond = "D->getBody() != NULL")
  getBody(Stmt s)[
    $s = "D->getBody()"
  ]

]

@node(name = D, targetType = "CXXMethodDecl *")
CXXMethodDecl extends FunctionDecl[

  @property(type = existential)
  isStatic()[
    "D->isStatic()"
  ]

  @property(type = existential)
  isInstance()[
    "D->isInstance()"
  ]

  @property(type = existential)
  isConst()[
    "D->isConst()"
  ]

  @property(type = existential)
  isVirtual()[
    "D->isVirtual()"
  ]

  @property(type = existential)
  isCopyAssignmentOperator()[
    "D->isCopyAssignmentOperator()"
  ]

  @property(type = many-to-one,
            cond = "D->getBody() != NULL")
  declaringClass(CXXRecordDecl c)[
    $c = "D->getParent()->getDefinition()"
  ]

  @property(type = many-to-one,
  	    cond = "D->getBody() != NULL")
  getThisType(QualType t)[
    $t = "D->getThisType( const_cast<ASTContext&>(*Context) )"
  ]

  @property(type = existential)
  hasInlineBody()[
    "D->hasInlineBody()"
  ]

  @property(type = many-to-many,
  	        targetType = "CXXMethodDecl::method_iterator",
            deref = true,
	    cond = "D->getBody() != NULL")
  hasOverriddenMethod(CXXMethodDecl meth)[
    $meth:start = "D->begin_overridden_methods()"
    $meth:end = "D->end_overridden_methods()"
  ]

]

@node(name = D, targetType = "CXXConstructorDecl *")
CXXConstructorDecl extends CXXMethodDecl[

  @property(type = existential)
  isDefault()[
    "D->isDefaultConstructor()"
  ]

  @property(type = existential)
  isCopy()[
    "D->isCopyConstructor()"
  ]

  @property(type = existential)
  isConverting()[
    "D->isConvertingConstructor(true)"
  ]

]

@node(name = D, targetType = "CXXConversionDecl *")
CXXConversionDecl extends CXXMethodDecl[

  @property(type = many-to-one)
  conversionType(QualType t)[
    $t = "D->getConversionType()"
  ]

]

@node(name = D, targetType = "CXXDestructorDecl *")
CXXDestructorDecl extends CXXMethodDecl[

  @property(type = many-to-one,
            cond = "D->getOperatorDelete() != NULL")
  deleteOperator(FunctionDecl f)[
    $f = "D->getOperatorDelete()"
  ]

]

@node(name = D, targetType = "VarDecl *")
VarDecl extends DeclaratorDecl[

/*
 * Is local variable (parameter or non-static function variable)
*/
  @property(type = existential)
  isLocal()[
    "D->hasLocalStorage()"
  ]

  @property(type = existential)
  isStaticLocal()[
    "D->isStaticLocal()"
  ]

  @property(type = existential)
  isStaticInFile()[
    "D->getStorageClass() == SC_Static && !D->isLocalVarDecl()"
  ]

/*
 * Is declared *inside* a function body.
 * That includes static local variables
 * and excludes function parameters.
 */
  @property(type = existential)
  isInFunctionBody()[
    "D->isLocalVarDecl()"
  ]

  @property(type = existential)
  isExternal()[
    "D->hasExternalStorage()"
  ]

/*
 * static variables inside a function
 * also have global storage
 */
  @property(type = existential)
  isGlobal()[
    "D->hasGlobalStorage() && !D->isLocalVarDecl()"
  ]

  @property(type = existential)
  isExternCVar()[
    "D->isExternC()"
  ]

  @property(type = existential)
  isStaticDataMember()[
    "D->isStaticDataMember()"
  ]

  @property(type = existential)
  isExceptionVariable()[
    "D->isExceptionVariable()"
  ]

  @property(type = many-to-one)
  definitionKind(DefinitionKind dk)[
    $dk = "D->isThisDeclarationADefinition()"
  ]

  @property(type = many-to-one,
            cond = "D->getDefinition() != NULL")
  getDefinition(VarDecl v)[
    $v = "D->getDefinition()"
  ]

]

@node(name = D, targetType = "ParmVarDecl *")
ParmVarDecl extends VarDecl[

  @property(type = many-to-one)
  index(int i)[
    $i = "D->getFunctionScopeIndex()"
  ]

  @property(type = existential)
  hasDefaultValue()[
    "D->hasDefaultArg()"
  ]

  @property(type = existential)
  hasInheritedDefaultValue()[
    "D->hasInheritedDefaultArg()"
  ]

]

@node(name = D, targetType = "TypedefNameDecl *")
TypedefNameDecl extends TypeDecl[

  @property(type = many-to-one,
  	    cond = "!D->getUnderlyingType().getTypePtr()->isDependentType() && !D->getUnderlyingType().getTypePtr()->isInstantiationDependentType()")
  underlyingType(QualType t)[
    $t = "D->getUnderlyingType()"
  ]
  
  @property(type = many-to-many,
  	    cond = "D->getUnderlyingType().getTypePtr()->isDependentType() || D->getUnderlyingType().getTypePtr()->isInstantiationDependentType()")
  hasMultipleUnderlyingType(QualType t)[
    $t = "D->getUnderlyingType()"
  ]

]

@node(name = D,
      targetType = "TagDecl *")
TagDecl extends TypeDecl[

  @property(type = many-to-one)
  tagKind(TagTypeKind tag)[
    $tag = "D->getTagKind()"
  ]

  @property(type = many-to-one,
            cond = "D->getDefinition() != NULL")
  getDefinition(TagDecl definition)[
    $definition = "D->getDefinition()"
  ]

  @property(type = existential)
  isNotADefinition()[
    "!D->isThisDeclarationADefinition()"
  ]

]

@node(name = D,
      targetType = "EnumConstantDecl *")
EnumConstantDecl extends ValueDecl[

  @property(type = many-to-one)
  value(int v)[
    $v = "D->getInitVal()"
  ]

]

@node(name = D,
      targetType = "EnumDecl *")
EnumDecl extends TagDecl[

  @property(type = many-to-many,
  	        targetType = "EnumDecl::enumerator_iterator",
            deref = true)
  hasEnumerator(EnumConstantDecl enumerator)[
    $enumerator:start = "D->enumerator_begin()"
    $enumerator:end = "D->enumerator_end()"
  ]

]

@node(name = D,
      targetType = "RecordDecl *")
RecordDecl extends TagDecl[

/*
 * To be an anonymous class, struct or union, it must have been declared without
 * a name and there must be no objects of this type declared, e.g.
 * union { int i; float f; };
 * NOTE: union X { int i; float f; }; OR union { int i; float f; } obj;
 * are *not* considered anonymous.
 */
  @property(type = existential)
  isAnonymous()[
    "D->isAnonymousStructOrUnion()"
  ]

  @property(type = many-to-many,
  	        targetType = "RecordDecl::field_iterator",
            deref = true)
  hasField(FieldDecl field)[
    $field:start = "D->field_begin()"
    $field:end = "D->field_end()"
  ]

]

@node(name = D,
      targetType = "CXXRecordDecl *")
CXXRecordDecl extends RecordDecl[

  @property(type = many-to-one,
  	    cond = "D->hasDefinition() && D->getDefinition() == D && D->getDescribedClassTemplate() != NULL")
  hasTemplateDecl(ClassTemplateDecl templDecl)[
    $templDecl = "D->getDescribedClassTemplate()"
  ]

  @property(type = many-to-many,
  	        targetType = "CXXRecordDecl::base_class_const_iterator",
            cond = "D->hasDefinition() && D->getDefinition() == D")
  hasBaseClass(BaseClass baseClass)[
    $baseClass:start = "D->bases_begin()"
    $baseClass:end = "D->bases_end()"
  ]

  @property(type = many-to-many,
  	        targetType = "CXXRecordDecl::base_class_const_iterator",
            cond = "D->hasDefinition() && D->getDefinition() == D")
  hasVirtualBaseClass(BaseClass vBaseClass)[
    $vBaseClass:start = "D->vbases_begin()"
    $vBaseClass:end = "D->vbases_end()"
  ]

  @property(type = many-to-many,
  	        targetType = "CXXRecordDecl::method_iterator",
            deref = true,
            cond = "D->hasDefinition() && D->getDefinition() == D")
  hasMethod(CXXMethodDecl meth)[
    $meth:start = "D->method_begin()"
    $meth:end = "D->method_end()"
  ]

  @property(type = many-to-many,
  	        targetType = "CXXRecordDecl::ctor_iterator",
            deref = true,
            cond = "D->hasDefinition() && D->getDefinition() == D")
  hasConstructor(CXXConstructorDecl ctor)[
    $ctor:start = "D->ctor_begin()"
    $ctor:end = "D->ctor_end()"
  ]

  @property(type = many-to-many,
  	        targetType = "CXXRecordDecl::conversion_iterator",
            deref = true,
            cond = "D->hasDefinition() && D->getDefinition() == D")
  hasConversion(CXXConversionDecl cnv)[
    $cnv:start = "D->conversion_begin()"
    $cnv:end = "D->conversion_end()"
  ]

  @property(type = many-to-many,
  	        targetType = "CXXRecordDecl::friend_iterator",
            deref = true,
            cond = "D->hasDefinition() && D->getDefinition() == D")
  hasFriend(FriendDecl friend)[
    $friend:start = "D->friend_begin()"
    $friend:end = "D->friend_end()"
  ]

/*
 * A class with no user-declared constructors,
 * no private or protected non-static data members,
 * no base classes, and no virtual functions
 */
  @property(type = existential)
  isAggregate()[
    "D->hasDefinition() && D->isAggregate()"
  ]

/*
 * A class is POD if it is an aggregate that has
 * no non-static non-POD data members, no reference data members,
 * no user-defined copy assignment operator and no user-defined destructor.
 * NOTE: that this is the C++ TR1 definition of POD.
 */
  @property(type = existential)
  isPOD()[
    "D->hasDefinition() && D->isPOD()"
  ]

  @property(type = existential)
  isCLike()[
    "D->hasDefinition() && D->isCLike()"
  ]

  @property(type = existential)
  isEmtpy()[
    "D->hasDefinition() && D->isEmpty()"
  ]

/*
 * A class is polymorphic if it contains or inherits a virtual function.
 */
  @property(type = existential)
  isPolymorphic()[
    "D->hasDefinition() && D->isPolymorphic()"
  ]

/*
 * A class is abstract if it declares a pure virtual function
 * or inherits a pure virtual function that is not overridden.
 */
  @property(type = existential)
  isAbstract()[
    "D->hasDefinition() && D->isAbstract()"
  ]

  @property(type = many-to-one,
            cond = "D->hasDefinition() && D->getDefinition() == D && D->getDestructor() != NULL && D->getDestructor()->isUserProvided()")
  hasDestructor(CXXDestructorDecl d)[
    $d = "dyn_cast<CXXDestructorDecl>(D->getDestructor()->getCanonicalDecl())"
  ]

  @property(type = existential)
  hasImplicitDestructor()[
    "D->hasDefinition() && D->getDestructor() != NULL && D->getDestructor()->isUserProvided()"
  ]

  @property(type = many-to-one,
            cond = "D->hasDefinition() && D->getDefinition() == D && D->isLocalClass() != NULL")
  isLocalInFunction(FunctionDecl func)[
    $func = "D->isLocalClass()"
  ]

]

@node(name = D, targetType = "NonTypeTemplateParmDecl *")
NonTypeTemplateParmDecl extends DeclaratorDecl[ ]

@node(name = D, targetType = "TemplateTypeParmDecl *")
TemplateTypeParmDecl extends TypeDecl[

  @property(type = many-to-one)
  index(int i)[
    $i = "D->getIndex()"
  ]

  @property(type = many-to-one,
            cond = "D->hasDefaultArgument()")
  hasDefaultArgument(QualType t)[
    $t = "D->getDefaultArgument()"
  ]

]

@node(name = D, targetType = "TemplateDecl *")
TemplateDecl extends NamedDecl[

  @property(type = many-to-many,
  	        targetType = "TemplateParameterList::iterator",
            deref = true,
            ordered = true)
  hasParameter(NamedDecl n)[
    $n:start = "D->getTemplateParameters()->begin()"
    $n:end = "D->getTemplateParameters()->end()"
  ]

]

@node(name = D, targetType = "TemplateTemplateParmDecl *")
TemplateTemplateParmDecl extends TemplateDecl[ ]

@node(name = D, targetType = "RedeclarableTemplateDecl *")
RedeclarableTemplateDecl extends TemplateDecl[

/*
 * template<typename T> struct X {
 *   template<typename U> void f(T, U);
 *   template<typename U> struct Inner;
 * };
 * The following are Member Specializations
 * template<> template<typename T> void X<int>::f(int, T);
 * template<> template<typename T> struct X<int>::Inner { };
 */
  @property(type = existential)
  isMemberSpecialization()[
    "D->isMemberSpecialization()"
  ]

]

@node(name = D, targetType = "ClassTemplateDecl *")
ClassTemplateDecl extends RedeclarableTemplateDecl[

  //@property(type = many-to-one)
  //underlyingFunctionDecl(CXXRecordDecl c)[
    //$c = "D->getTemplatedDecl()"
  //]

  //@property(type = many-to-one,
            //cond = "D->getInstantiatedFromMemberTemplate() != NULL")
  //instantiatedFrom(ClassTemplateDecl f)[
    //$f = "D->getInstantiatedFromMemberTemplate()"
  //]

/*
 * The injected-class-name for a class template X is X<template-args>,
 * where template-args is formed from the template arguments that
 * correspond to the template parameters of X. For example:
 * template<typename T, int N> struct array {
 *   typedef array this_type; // "array" is equivalent to "array<T, N>"
 * };
 */
  //@property(type = many-to-one)
  //injectedClassSpecialization(QualType t)[
    //$t = "D->getInjectedClassNameSpecialization()"
  //]

  //@property(type = many-to-many,
  	        //targetType = "ClassTemplateDecl::spec_iterator",
            //deref = true)
  //hasSpecialization(ClassTemplateSpecializationDecl c)[
    //$c:start = "D->spec_begin()"
    //$c:end = "D->spec_end()"
  //]

  //@property(type = many-to-many,
  	        //targetType = "ClassTemplateDecl::partial_spec_iterator",
            //deref = true)
  //hasPartialSpecialization(ClassTemplatePartialSpecializationDecl c)[
    //$c:start = "D->partial_spec_begin()"
    //$c:end = "D->partial_spec_end()"
  //]

]

//@node(name = D, targetType = "FunctionTemplateDecl *")
//FunctionTemplateDecl extends RedeclarableTemplateDecl[

  //@property(type = many-to-one)
  //underlyingFunctionDecl(FunctionDecl f)[
    //$f = "D->getTemplatedDecl()"
  //]

  //@property(type = many-to-one,
            //cond = "D->getInstantiatedFromMemberTemplate() != NULL")
  //instantiatedFrom(FunctionTemplateDecl f)[
    //$f = "D->getInstantiatedFromMemberTemplate()"
  //]

/*
 * template<typename T1, typename T2> void f(T1, T2);
 * template<> void f<int, int>(int, int); // function template specialization
 */
  //@property(type = many-to-many,
  //	        targetType = "FunctionTemplateDecl::spec_iterator",
    //        deref = true)
  //hasSpecialization(FunctionDecl f)[
    //$f:start = "D->spec_begin()"
    //$f:end = "D->spec_end()"
  //]

//]

/*
 * template<typename T> class array;
 * template<> class array<bool> { }; // class template specialization array<bool>
 */
@node(name = D, targetType = "ClassTemplateSpecializationDecl *")
ClassTemplateSpecializationDecl extends CXXRecordDecl[

  /*@property(type = many-to-one)
  hasType(QualType t)[
    $t = "D->getTypeAsWritten()->getType()"
  ]

  @property(type = many-to-many,
  	        targetType = "llvm::ArrayRef<TemplateArgument>::iterator")
  hasTemplateArgument(TemplateArgument a)[
    $a:start = "D->getTemplateArgs().asArray().begin()"
    $a:end = "D->getTemplateArgs().asArray().end()"
  ]*/

]

@node(name = D, targetType = "ClassTemplatePartialSpecializationDecl *")
ClassTemplatePartialSpecializationDecl extends ClassTemplateSpecializationDecl[

  @property(type = many-to-many,
  	        targetType = "TemplateParameterList::iterator",
            deref = true,
            ordered = true)
  hasParameter(NamedDecl n)[
    $n:start = "D->getTemplateParameters()->begin()"
    $n:end = "D->getTemplateParameters()->end()"
  ]

]


@node(name = S, targetType = "const Stmt *")
Stmt[

  @constructor(uid)

  @property(type = many-to-one)
  uid(int i)[
    $i = "getUID( S )"
  ]

  @property(type = many-to-one,
            cond = "getSourceFile(SrcManager.getPresumedLoc(S->getLocStart(), false)) != NULL")
  sourceFile(FileEntry sourcefile)[
    $sourcefile = "getSourceFile(SrcManager.getPresumedLoc(S->getLocStart(), false))"
  ]

  @property(type = many-to-one)
  sourceLocationStart(Location locationStart)[
    $locationStart = "getSourceLocation(Location::BEGIN, S)"
  ]

  @property(type = many-to-one)
  sourceLocationEnd(Location locationEnd)[
    $locationEnd = "getSourceLocation(Location::END, S)"
  ]

  @property(type = many-to-many,
  	        targetType = "Stmt::const_child_iterator",
            deref = true)
  hasChild(Stmt c)[
    $c:start = "S->child_begin()"
    $c:end = "S->child_end()"
  ]

]

@node(name = S, targetType = "BreakStmt *")
BreakStmt extends Stmt[ ]

@node(name = S, targetType = "CompoundStmt *")
CompoundStmt extends Stmt[ ]

@node(name = S, targetType = "ContinueStmt *")
ContinueStmt extends Stmt[ ]

@node(name = S,
      targetType = "DeclStmt *")
DeclStmt extends Stmt[

  @property(type = many-to-many,
  	        targetType = "DeclStmt::decl_iterator",
            deref = true)
  hasDecl(Decl d)[
    $d:start = "S->decl_begin()"
    $d:end = "S->decl_end()"
  ]

]

@node(name = S, targetType = "CXXCatchStmt *")
CXXCatchStmt extends Stmt[

  /*@property(type = many-to-one)
  exceptionVariable(VarDecl v)[
    $v = "S->getExceptionDecl()"
  ]

  @property(type = many-to-one)
  caughtType(QualType t)[
    $t = "S->getCaughtType()"
  ]

  @property(type = many-to-one)
  handlerBlock(Stmt s)[
    $s = "S->getHandlerBlock()"
  ]*/

]

@node(name = S, targetType = "CXXTryStmt *")
CXXTryStmt extends Stmt[

  @property(type = many-to-one)
  tryBlock(CompoundStmt s)[
    $s = "S->getTryBlock()"
  ]

  @property(type = many-to-many,
  	        targetType = "std::vector<CXXCatchStmt *>::iterator",
            deref = true,
            /* Dummy condition just to initialize the wrap variable */
            cond = "HandlerWrap* wrap = new HandlerWrap( new CatchHandlerFunc(S) )")
  hasHandler(CXXCatchStmt s)[
    $s:start = "wrap->vec.begin()"
    $s:end = "wrap->vec.end()"
  ]

]

@node(name = S, targetType = "GotoStmt *")
GotoStmt extends Stmt[

  @property(type = many-to-one)
  getLabel(LabelDecl l)[
    $l = "S->getLabel()"
  ]

  @property(type = many-to-one)
  getLabelStmt(LabelStmt s)[
    $s = "S->getLabel()->getStmt()"
  ]

]

@node(name = S, targetType = "IndirectGotoStmt *")
IndirectGotoStmt extends Stmt[

  @property(type = many-to-one)
  getTarget(Expr e)[
    $e = "S->getTarget()"
  ]

]


@node(name = S, targetType = "LabelStmt *")
LabelStmt extends Stmt[

  @property(type = many-to-one)
  getLabel(LabelDecl l)[
    $l = "S->getDecl()"
  ]

  @property(type = many-to-one)
  getLabelName(string l)[
    $l = "std::string( S->getName() )"
  ]

  @property(type = many-to-one)
  getSubStmt(Stmt s)[
    $s = "S->getSubStmt()"
  ]

]

@node(name = S, targetType = "DoStmt *")
DoStmt extends Stmt[

  @property(type = many-to-one)
  getCondition(Expr e)[
    $e = "S->getCond()"
  ]

  @property(type = many-to-one)
  getBody(Stmt s)[
    $s = "S->getBody()"
  ]

]

@node(name = S, targetType = "WhileStmt *")
WhileStmt extends Stmt[

  @property(type = many-to-one)
  getCondition(Expr e)[
    $e = "S->getCond()"
  ]

  @property(type = many-to-one)
  getBody(Stmt s)[
    $s = "S->getBody()"
  ]

  @property(type = many-to-one,
            cond = "S->getConditionVariable() != NULL")
  getConditionVariable(VarDecl v)[
    $v = "S->getConditionVariable()"
  ]

]

@node(name = S, targetType = "ForStmt *")
ForStmt extends Stmt[

  @property(type = many-to-one)
  getInitialization(Stmt s)[
    $s = "S->getInit()"
  ]

  @property(type = many-to-one)
  getCondition(Expr e)[
    $e = "S->getCond()"
  ]

  @property(type = many-to-one)
  getIncrement(Expr e)[
    $e = "S->getInc()"
  ]

  @property(type = many-to-one)
  getBody(Stmt s)[
    $s = "S->getBody()"
  ]

/*
 * NOTE: In the following example, "y" is the condition variable.
 * for (int x = random(); int y = mangle(x); ++x) { ... }
 */
  @property(type = many-to-one,
            cond = "S->getConditionVariable() != NULL")
  getConditionVariable(VarDecl v)[
    $v = "S->getConditionVariable()"
  ]

]

@node(name = S, targetType = "IfStmt *")
IfStmt extends Stmt[

  @property(type = many-to-one)
  getCondition(Expr e)[
    $e = "S->getCond()"
  ]

  @property(type = many-to-one)
  getThen(Stmt s)[
    $s = "S->getThen()"
  ]

  @property(type = many-to-one)
  getElse(Stmt s)[
    $s = "S->getElse()"
  ]

  @property(type = many-to-one,
            cond = "S->getConditionVariable() != NULL")
  getConditionVariable(VarDecl v)[
    $v = "S->getConditionVariable()"
  ]

]

@node(name = S, targetType = "ReturnStmt *")
ReturnStmt extends Stmt[

  @property(type = many-to-one,
            cond = "S->getRetValue()")
  getValue(Expr e)[
    $e = "S->getRetValue()"
  ]

]

@node(name = S, targetType = "SwitchCase *")
SwitchCase extends Stmt[

  @property(type = many-to-one,
            cond = "S->getNextSwitchCase() != NULL")
  isAfterCase(SwitchCase c)[
    $c = "S->getNextSwitchCase()"
  ]

  @property(type = many-to-one)
  getBody(Stmt s)[
    $s = "S->getSubStmt()"
  ]

]

@node(name = S, targetType = "DefaultStmt *")
DefaultStmt extends SwitchCase[ ]

@node(name = S, targetType = "CaseStmt *")
CaseStmt extends SwitchCase[

  @property(type = many-to-one)
  getValue(Expr e)[
    $e = "S->getLHS()"
  ]

]

@node(name = S, targetType = "SwitchStmt *")
SwitchStmt extends Stmt[

  @property(type = many-to-one)
  getCond(Expr e)[
    $e = "S->getCond()"
  ]

  @property(type = many-to-one,
            cond = "S->getConditionVariable() != NULL")
  getConditionVariable(VarDecl v)[
    $v = "S->getConditionVariable()"
  ]

  @property(type = many-to-one)
  getBody(Stmt s)[
    $s = "S->getBody()"
  ]

]


@node(name = S, targetType = "Expr *")
Expr extends Stmt[

/*
 * Declared as many-to-many because an expression might have
 * more than one possible types. E.g.
 * int a ; if ( a ) ...
 * The expression in the if condition has both an integer and a boolean type
 */
  @property(type = many-to-many,
            cond = "S->getType().getTypePtrOrNull() != NULL")
  hasType(QualType t)[
    $t = "S->getType()"
  ]

/*
 * Determines whether this expression is value-dependent.
 * For example, the array bound of "Chars" in the following example is value-dependent
 * template <int Size, char (&Chars)[Size]> struct meta_string;
 */
  @property(type = existential)
  isValueDependent()[
    "S->isValueDependent()"
  ]

/*
 * Determines whether this expression is type-dependent, which means that
 * its type could change from one template instantiation to the next.
 * For example, the expressions "x" and "x + y" are type-dependent in the
 * following code, but "y" is not type-dependent:
 * template<typename T> void add(T x, int y) { x + y; }
 */
  @property(type = existential)
  isTypeDependent()[
    "S->isTypeDependent()"
  ]

/*
 * Determined whether this expression is instantiation-dependent,
 * meaning that it depends in some way on a template parameter,
 * even if neither its type nor (constant) value can change due
 * to the template instantiation.
 * In the following example, the expression sizeof(sizeof(T() + T()))
 * is instantiation-dependent (since it involves a template parameter T),
 * but is neither type- nor value-dependent, since the type of the
 * inner sizeof is known (std::size_t) and therefore the size of the outer sizeof is known.
 * template<typename T> void f(T x, T y) { sizeof(sizeof(T() + T()); }
 */
  @property(type = existential)
  isInstantiationDependent()[
    "S->isInstantiationDependent()"
  ]

  @property(type = existential)
  isLValue()[
    "S->isLValue()"
  ]

  @property(type = existential)
  isRValue()[
    "S->isRValue()"
  ]

  @property(type = existential)
  isBooleanExpr()[
    "S->getType().getTypePtrOrNull() != NULL && S->isKnownToHaveBooleanValue()"
  ]

/*
  @property(type = existential)
  isIntegerConstantExpr()[
    "S->isIntegerConstantExpr(*Context)"
  ]

  @property(type = existential)
  isConstantInitializer()[
    "S->isConstantInitializer(*(const_cast<ASTContext *>(Context)), true)"
  ]

  @property(type = existential)
  isEvaluatable()[
    "S->isEvaluatable(*Context)"
  ]

  @property(type = existential)
  isNullConstant()[
    "S->getType().getTypePtrOrNull() != NULL && S->isNullPointerConstant(*(const_cast<ASTContext *>(Context)), Expr::NPC_NeverValueDependent) != Expr::NPCK_NotNull"
  ]
*/

  /*@property(type = existential)
  hasSideEffects()[
    "S->HasSideEffects(*Context)"
  ]*/

  @property(type = existential)
  hasNonTrivialCall()[
    "S->hasNonTrivialCall(*(const_cast<ASTContext *>(Context)))"
  ]

  @property(type = existential)
  isDefaultArgument()[
    "S->isDefaultArgument()"
  ]

/* Whether this expression is an implicit reference to 'this' in C++ */
  @property(type = existential)
  isImplicitCXXThis()[
    "S->isImplicitCXXThis()"
  ]

/*
 * Return the most derived class decl the expression is known to refer to.
 * If this expression is a cast, this method looks through it to find the
 * most derived decl that can be inferred from the expression.
 * This is valid because derived-to-base conversions have
 * undefined behavior if the object isn't dynamically of the derived type.
 */
/*
  @property(type = many-to-one,
            cond = "S->getType().getTypePtrOrNull() !=NULL && S->getBestDynamicClassType() != NULL")
  getBestDynamicClassType(CXXRecordDecl c)[
    $c = "S->getBestDynamicClassType()"
  ]
*/

]

@node(name = S, targetType = "ConditionalOperator *")
ConditionalOperator extends Expr[

  @property(type = many-to-one)
  condition(Expr e)[
    $e = "S->getCond()"
  ]

  @property(type = many-to-one)
  truePath(Expr e)[
    $e = "S->getTrueExpr()"
  ]

  @property(type = many-to-one)
  falsePath(Expr e)[
    $e = "S->getFalseExpr()"
  ]

]

@node(name = S, targetType = "ArraySubscriptExpr *")
ArraySubscriptExpr extends Expr[

/*
 * NOTE: An array access can be written A[4] or 4[A] (both are equivalent).
 * getBase() and getIdx() always present the normalized view: A[4].
 * In this case getBase() returns "A" and getIdx() returns "4".
 * Use getLHS() and getRHS() to get the syntactic view.
*/
  @property(type = many-to-one)
  base(Expr e)[
    $e = "S->getBase()"
  ]

  @property(type = many-to-one)
  index(Expr e)[
    $e = "S->getIdx()"
  ]

]

@node(name = S, targetType = "BinaryOperator *")
BinaryOperator extends Expr[

  @property(type = many-to-one)
  opcode(BinaryOperatorKind op)[
    $op = "S->getOpcode()"
  ]

  @property(type = many-to-one)
  hasLHS(Expr e)[
    $e = "S->getLHS()"
  ]

  @property(type = many-to-one)
  hasRHS(Expr e)[
    $e = "S->getRHS()"
  ]

]

@node(name = S, targetType = "CharacterLiteral *")
CharacterLiteral extends Expr[

  @property(type = many-to-one)
  hasValue(int i)[
    $i = "S->getValue()"
  ]

  @property(type = many-to-one)
  hasKind(CharacterKind k)[
    $k = "S->getKind()"
  ]

]

@node(name = S, targetType = "CallExpr *")
CallExpr extends Expr[

  @property(type = many-to-one)
  getCalleeExpr(Expr e)[
    $e = "S->getCallee()"
  ]

  @property(type = many-to-one,
            cond = "S->getCalleeDecl() != NULL")
  hasCalleeDecl(Decl d)[
    $d = "S->getCalleeDecl()"
  ]

  @property(type = many-to-many,
  	        targetType = "const Expr* const*",
            deref = true,
            ordered = true)
  hasArgumentExpr(Expr e)[
    $e:start = "S->getArgs()"
    $e:end = "S->getArgs() + S->getNumArgs()"
  ]

]

@node(name = S, targetType = "CXXMemberCallExpr *")
CXXMemberCallExpr extends CallExpr[

  @property(type = many-to-one)
  receiverObjectExpr(Expr e)[
    $e = "S->getImplicitObjectArgument()"
  ]

  @property(type = many-to-one)
  method(CXXMethodDecl m)[
    $m = "S->getMethodDecl()"
  ]

]

@node(name = S, targetType = "CXXOperatorCallExpr *")
CXXOperatorCallExpr extends CallExpr[ ]

@node(name = S, targetType = "CastExpr *")
CastExpr extends Expr[

  @property(type = many-to-one)
  getSubExpr(Expr e)[
    $e = "S->getSubExprAsWritten()"
  ]

  @property(type = many-to-one)
  getSubExprWithImplicit(Expr e)[
    $e = "S->getSubExpr()"
  ]

]

@node(name = S, targetType = "ImplicitCastExpr *")
ImplicitCastExpr extends CastExpr[ ]

@node(name = S, targetType = "ExplicitCastExpr *")
ExplicitCastExpr extends CastExpr[

  @property(type = many-to-many)
  hasCastType(QualType t)[
    $t = "S->getTypeAsWritten()"
  ]

]

@node(name = S, targetType = "CStyleCastExpr *")
CStyleCastExpr extends ExplicitCastExpr[ ]

/* C++ type conversion that uses "functional" notation. e.g. x = int(0.5); */
@node(name = S, targetType = "CXXFunctionalCastExpr *")
CXXFunctionalCastExpr extends ExplicitCastExpr[ ]

@node(name = S, targetType = "CXXNamedCastExpr *")
CXXNamedCastExpr extends ExplicitCastExpr[ ]

@node(name = S, targetType = "CXXConstCastExpr *")
CXXConstCastExpr extends CXXNamedCastExpr[ ]

@node(name = S, targetType = "CXXDynamicCastExpr *")
CXXDynamicCastExpr extends CXXNamedCastExpr[ ]

@node(name = S, targetType = "CXXReinterpretCastExpr *")
CXXReinterpretCastExpr extends CXXNamedCastExpr[ ]

@node(name = S, targetType = "CXXStaticCastExpr *")
CXXStaticCastExpr extends CXXNamedCastExpr[ ]

@node(name = S, targetType = "CXXBoolLiteralExpr *")
CXXBoolLiteralExpr extends Expr[

  @property(type = existential)
  isTrue()[
    "S->getValue()"
  ]

]

@node(name = S, targetType = "CXXThisExpr *")
CXXThisExpr extends Expr[

  @property(type = existential)
  isImplicit()[
    "S->isImplicit()"
  ]

]

@node(name = S, targetType = "CXXThrowExpr *")
CXXThrowExpr extends Expr[

/*
 * NOTE: in throw 42; (i.e. a constant) the method returns false
 */
  @property(type = existential)
  isThrownVariableInScope()[
    "S->isThrownVariableInScope()"
  ]

/*
 * Conditional in case of "throw;" with no variable / literal
 */
  @property(type = many-to-one,
            cond = "S->getSubExpr() != NULL")
  getSubExpr(Expr e)[
    $e = "S->getSubExpr()"
  ]

]

@node(name = S, targetType = "FloatingLiteral *")
FloatingLiteral extends Expr[

// TODO: Add a float or decimal type to the specification
  @property(type = many-to-one)
  getValue(string d)[
    $d = "S->getValueAsApproximateDouble()"
  ]

]

@node(name = S, targetType = "IntegerLiteral *")
IntegerLiteral extends Expr[

  @property(type = many-to-one)
  getValue(int i)[
    $i = "S->getValue().toString(10, true)"
  ]

]

@node(name = S, targetType = "StringLiteral *")
StringLiteral extends Expr[

  @property(type = many-to-one)
  getValue(string s)[
    $s = "escapeForCSV( S->getBytes() )"
  ]

  @property(type = many-to-one)
  getLength(int s)[
    $s = "S->getLength()"
  ]

]

@node(name = S, targetType = "ParenExpr *")
ParenExpr extends Expr[

  @property(type = many-to-one)
  getSubExpr(Expr e)[
    $e = "S->getSubExpr()"
  ]

]

@node(name = S, targetType = "UnaryOperator *")
UnaryOperator extends Expr[

  @property(type = many-to-one)
  getKind(UnaryOperatorKind k)[
    $k = "S->getOpcode()"
  ]

  @property(type = many-to-one)
  getSubExpr(Expr e)[
    $e = "S->getSubExpr()"
  ]

]

@node(name = S, targetType = "UnaryExprOrTypeTraitExpr *")
UnaryExprOrTypeTraitExpr extends Expr[

/*
 * Used for sizeof, alignof and vec_step (OpenCL)
 */
  @property(type = many-to-one)
  getKind(UnaryExprOrTypeTrait k)[
    $k = "S->getKind()"
  ]

/*
 * Argument is a type. E.g. sizeof(class A)
 */
  @property(type = many-to-one,
            cond = "S->isArgumentType()")
  getTypeArgument(QualType t)[
    $t = "S->getArgumentType()"
  ]

/*
 * Argument is an expression. E.g. sizeof(a)
 */
  @property(type = many-to-one,
            cond = "!S->isArgumentType()")
  getExprArgument(Expr e)[
    $e = "S->getArgumentExpr()"
  ]

/*
 * The type of the argument.
 * E.g in both cases sizeof(class A) and sizeof(a) the type is "class A"
 */
  @property(type = many-to-one)
  getTypeOfArgument(QualType t)[
    $t = "S->getTypeOfArgument()"
  ]

]

/*
 * Represents an rvalue temporary that is written into memory so that a reference can bind to it.
 * e.g. const int &r = 1.0;
 */
@node(name = S, targetType = "MaterializeTemporaryExpr *")
MaterializeTemporaryExpr extends Expr[

  @property(type = many-to-one)
  getTemporaryExpr(Expr e)[
    $e = "S->GetTemporaryExpr()"
  ]

/*
 * Get the declaration which triggered the lifetime-extension of this temporary, if any.
 */
  @property(type = many-to-one,
            cond = "S->getExtendingDecl() != NULL")
  getExtendingDecl(ValueDecl v)[
    $v = "S->getExtendingDecl()"
  ]

]

/*
 * Represents an expression that introduces cleanups to be run at the end of the sub-expression's evaluation.
 * The most common source of expression-introduced cleanups is temporary objects in C++.
 */
@node(name = S, targetType = "ExprWithCleanups *")
ExprWithCleanups extends Expr[

  @property(type = many-to-one)
  getSubExpr(Expr e)[
    $e = "S->getSubExpr()"
  ]

]

@node(name = S, targetType = "CXXTypeidExpr *")
CXXTypeidExpr extends Expr[

  @property(type = many-to-one,
            cond = "S->isTypeOperand()")
  getTypeOperand(QualType t)[
    $t = "S->getTypeOperand(*const_cast<ASTContext*>(Context))"
  ]

  @property(type = many-to-one,
            cond = "!S->isTypeOperand()")
  getExprOperand(Expr e)[
    $e = "S->getExprOperand()"
  ]

]

/*
 * Describes an explicit type conversion that uses functional notion
 * but could not be resolved because one or more arguments are type-dependent.
 * For example, this would occur in a template such as:
 * template<typename T, typename A1> inline T make_a(const A1& a1) { return T(a1); }
 * When the returned expression is instantiated, it may resolve to a constructor call,
 * conversion function call, or some kind of type conversion.
 */
@node(name = S, targetType = "CXXUnresolvedConstructExpr *")
CXXUnresolvedConstructExpr extends Expr[

  @property(type = many-to-one)
  hasType(QualType t)[
    $t = "S->getTypeAsWritten()"
  ]

  @property(type = many-to-many,
  	        targetType = "CXXUnresolvedConstructExpr::arg_iterator",
            deref = true,
            ordered = true)
  hasArgument(Expr e)[
    $e:start = "S->arg_begin()"
    $e:end = "S->arg_end()"
  ]

]

/*
 * An expression "T()" which creates a value-initialized rvalue of type T, which is a *non-class* type.
 */
@node(name = S, targetType = "CXXScalarValueInitExpr *")
CXXScalarValueInitExpr extends Expr[

  @property(type = many-to-one)
  hasType(QualType t)[
    $t = "S->getTypeSourceInfo()->getType()"
  ]

]

/*
 * A pseudo-destructor is an expression that looks like a member access to a destructor of a scalar type,
 * except that scalar types don't have destructors. For example:
 *   typedef int T;
 *   void f(int *p) { p->T::~T(); }
 * Pseudo-destructors typically occur when instantiating templates such as:
 *   template<typename T> void destroy(T* ptr) { ptr->T::~T(); }
 * for scalar types. A pseudo-destructor expression has no run-time semantics beyond evaluating the base expression.
 */
@node(name = S, targetType = "CXXPseudoDestructorExpr *")
CXXPseudoDestructorExpr extends Expr[

  @property(type = many-to-one)
  getBaseExpr(Expr e)[
    $e = "S->getBase()"
  ]

  @property(type = many-to-one)
  hasType(QualType t)[
    $t = "S->getDestroyedType()"
  ]

]

/*
 * This wraps up a function call argument that was created from the corresponding parameter's
 * default argument, when the *call* did not explicitly supply arguments for all of the parameters.
 */
@node(name = S, targetType = "CXXDefaultArgExpr *")
CXXDefaultArgExpr extends Expr[

  @property(type = many-to-one)
  forArgument(ParmVarDecl v)[
    $v = "S->getParam()"
  ]

  @property(type = many-to-one)
  getExpr(Expr e)[
    $e = "S->getExpr()"
  ]

]

@node(name = S, targetType = "CXXDeleteExpr *")
CXXDeleteExpr extends Expr[

  @property(type = existential)
  isArrayDelete()[
    "S->isArrayForm()"
  ]

  @property(type = existential)
  isGlobalDelete()[
    "S->isGlobalDelete()"
  ]

  @property(type = many-to-one,
            cond = "S->getOperatorDelete() != NULL")
  deleteOperator(FunctionDecl f)[
    $f = "S->getOperatorDelete()"
  ]

  @property(type = many-to-one)
  getArgument(Expr e)[
    $e = "S->getArgument()"
  ]

  @property(type = many-to-one,
            cond = "S->getDestroyedType().getTypePtrOrNull() != NULL")
  hasType(QualType t)[
    $t = "S->getDestroyedType()"
  ]

]

/*
 * Represents binding an expression to a temporary. This ensures the destructor is called
 * for the temporary. It should only be needed for non-POD, non-trivially destructable class types.
 *   struct S { S() {} ~S() {} };
 *   void test() { const S &s_ref = S(); } // Requires a CXXBindTemporaryExpr
 */
@node(name = S, targetType = "CXXBindTemporaryExpr *")
CXXBindTemporaryExpr extends Expr[

  @property(type = many-to-one)
  getDestructor(FunctionDecl f)[
    $f = "S->getTemporary()->getDestructor()"
  ]

  @property(type = many-to-many)
  getSubExpr(Expr e)[
    $e = "S->getSubExpr()"
  ]

]

@node(name = S, targetType = "CXXConstructExpr *")
CXXConstructExpr extends Expr[

  @property(type = many-to-one)
  getConstructor(CXXConstructorDecl c)[
    $c = "S->getConstructor()"
  ]

  @property(type = many-to-one)
  getConstructionKind(ConstructionKind k)[
    $k = "S->getConstructionKind()"
  ]

/* Whether this constructor call was written as list-initialization */
  @property(type = existential)
  isListInitialization()[
    "S->isListInitialization()"
  ]

  @property(type = existential)
  isElidable()[
    "S->isElidable()"
  ]

  @property(type = many-to-many,
  	        targetType = "const Expr* const*",
            deref = true,
            ordered = true)
  hasArgument(Expr e)[
    $e:start = "S->getArgs()"
    $e:end = "S->getArgs() + S->getNumArgs()"
  ]

]

/*
 * This expression type represents a C++ "functional" cast with N != 1 arguments
 * that invokes a constructor to build a temporary object.
 */
@node(name = S, targetType = "CXXTemporaryObjectExpr *")
CXXTemporaryObjectExpr extends CXXConstructExpr[ ]

@node(name = S, targetType = "CXXNewExpr *")
CXXNewExpr extends Expr[

  @property(type = existential)
  isArrayNew()[
    "S->isArray()"
  ]

  @property(type = existential)
  isGlobalNew()[
    "S->isGlobalNew()"
  ]

  @property(type = many-to-one,
            cond = "S->getOperatorNew() != NULL")
  newOperator(FunctionDecl f)[
    $f = "S->getOperatorNew()"
  ]

  @property(type = many-to-one,
            cond = "S->getOperatorDelete() != NULL")
  deleteOperator(FunctionDecl f)[
    $f = "S->getOperatorDelete()"
  ]

  @property(type = many-to-one,
            cond = "S->isArray()")
  hasArraySizeExpr(Expr e)[
    $e = "S->getArraySize()"
  ]

  @property(type = many-to-one)
  hasType(QualType t)[
    $t = "S->getAllocatedType()"
  ]

  @property(type = many-to-one,
            cond = "S->hasInitializer()")
  hasInitializer(Expr e)[
    $e = "S->getInitializer()"
  ]

  @property(type = many-to-one)
  hasInitializationStyle(InitializationStyle s)[
    $s = "S->getInitializationStyle()"
  ]

  @property(type = many-to-one)
  getConstructExpr(Expr e)[
    $e = "S->getConstructExpr()"
  ]

  @property(type = many-to-many,
  	        targetType = "const Expr* const*",
            deref = true,
            ordered = true)
  hasPlacementArgument(Expr e)[
    $e:start = "S->getPlacementArgs()"
    $e:end = "S->getPlacementArgs() + S->getNumPlacementArgs()"
  ]

]


@entity(name = F, targetType = "const FileEntry *")
FileEntry[

  @constructor(pathname)

  @property(type = one-to-one)
  pathname(string pathname)[
    $pathname = "getAbsoluteFilepath( F )"
  ]

  @property(type = many-to-one)
  size(int size)[
    $size = "F->getSize()"
  ]

  @property(type = many-to-one)
  modificationTime(int time)[
    $time = "F->getModificationTime()"
  ]

  @property(type = many-to-one)
  inDirectory(string dir)[
    $dir = "getAbsoluteFilepath( F->getDir() )"
  ]

  @property(type = many-to-many,
            cond = "isIncludedFrom( F ) != NULL")
  isIncludedFrom(FileEntry incFile)[
    $incFile = "isIncludedFrom( F )"
  ]

  @property(type = many-to-one)
  characteristic(CharacteristicKind ck)[
    $ck = "SrcManager.getFileCharacteristic( SrcManager.translateFileLineCol(F, 1, 0) )"
  ]

]

@entity(name = L, targetType = "Location")
Location[

  @constructor(getVal)

  @property(type = one-to-one)
  getVal(string loc)[
    $loc = "L.loc"
  ]

  @property(type = many-to-one,
  	    cond = "L.filename != NULL")
  inFile(FileEntry filename)[
    $filename = "L.filename"
  ]

]

@entity(name = B, targetType = "const CXXBaseSpecifier *")
BaseClass[
  /*
   * FIXME: This is wrong
   */
  @constructor(baseType)

  @property(type = many-to-one)
  baseType(QualType bt)[
    $bt = "B->getType()"
  ]

  @property(type = existential)
  isVirtual()[
    "B->isVirtual()"
  ]

  @property(type = many-to-one)
  accessSpecifier(AccessSpecifier as)[
    $as = "B->getAccessSpecifier()"
  ]

]

@entity(name = N, targetType = "const NestedNameSpecifier *")
NestedNameSpecifier[

  @constructor(uid)

  @property(type = many-to-one)
  uid(int i)[
    $i = "getUID( N )"
  ]

  @property(type = many-to-one,
            cond = "N->getPrefix() != NULL")
  hasPrefix(NestedNameSpecifier n)[
    $n = "N->getPrefix()"
  ]

  @property(type = many-to-one,
            cond = "N->getKind() == NestedNameSpecifier::Identifier")
  asIdentifier(string id)[
    $id = "std::string( N->getAsIdentifier()->getNameStart() )"
  ]

  @property(type = many-to-one,
            cond = "N->getKind() == NestedNameSpecifier::Namespace")
  asNamespace(NamespaceDecl ns)[
    $ns = "N->getAsNamespace()"
  ]

  @property(type = many-to-one,
            cond = "N->getKind() == NestedNameSpecifier::NamespaceAlias")
  asNamespaceAlias(NamespaceAliasDecl ns)[
    $ns = "N->getAsNamespaceAlias()"
  ]

  @property(type = many-to-one,
            cond = "N->getKind() == NestedNameSpecifier::TypeSpec")
  asType(Type t)[
    $t = "N->getAsType()"
  ]

  @property(type = many-to-one,
            cond = "N->getKind() == NestedNameSpecifier::TypeSpecWithTemplate")
  asTemplateType(Type t)[
    $t = "N->getAsType()"
  ]

  @property(type = existential)
  asGlobal()[
    "N->getKind() == NestedNameSpecifier::Global"
  ]

  @property(type = existential)
  isDependent()[
    "N->isDependent()"
  ]

]

//Types and Qualified Types

/*
 * Note: Some TypeClasses are not
 * visited by the RecursiveASTVisitor
 * (various FIXME comments inside the class).
 * So for these TypeClasses we don't have all the
 * appropriate information yet.
 */

@node(name = Tp,
      targetType = "const Type *",
      cache = true)
Type[

  @constructor(uid)
  //Just for debugging
  @property(type = many-to-one)
  kind(string s)[
    $s = "string(Tp->getTypeClassName())"
  ]

  @property(type = many-to-one)
  uid(int i)[
    $i = "getUID( Tp )"
  ]

  @property(type = one-to-one)
  toString(string str)[
    $str = "getAsString(Tp)"
  ]

]

@node(name = ElTp,
      targetType = "const ElaboratedType *")
ElaboratedType extends Type[

  @property(type = many-to-one)
  hasNamedType(QualType nTp)[
    $nTp = "ElTp->getNamedType()"
  ]

]

//ArrayType hierarchy
@node(name = ArType,
      targetType = "const ArrayType *")
ArrayType extends Type[

  @property(type = many-to-one)
  hasSizeModifier(ArraySizeModifier modifier)[
    $modifier = "ArType->getSizeModifier()"
  ]

  @property(type = many-to-one)
  hasElementType(QualType qType)[
    $qType = "ArType->getElementType()"
  ]

  @property(type = many-to-one)
  hasIndexTypeQualifiers(Qualifiers quals)[
    $quals = "ArType->getIndexTypeQualifiers()"
  ]

]

@node(name = ConstAr,
	targetType = "const ConstantArrayType*")
ConstantArrayType extends ArrayType[

  @property(type = many-to-one)
  hasSize(int sz)[
    $sz = "ConstAr->getSize()"
  ]

]

@node(name = DepSzAr,
      targetType = "const DependentSizedArrayType *")
DependentSizedArrayType extends ArrayType[

  /* 
   * TODO: Add the Expr, when we will have cons
   * for Expr and Stmt. Also I should probably add this
   * as salt while hashing.
   */

]

@node(name = IncArTp,
	targetType = "const IncompleteArrayType *")
IncompleteArrayType extends ArrayType[
  //All informationIncompleteArrayType is inside the ArrayType.
]

@node(name = VarArTp,
      targetType = "const VariableArrayType *")
VariableArrayType extends ArrayType[

  /* 
   * TODO: Add the Expr, when we will have cons
   * for Expr and Stmt. Also I should probably add this
   * as salt while hashing.
   */
]

//End ArrayType hierarchy

@node(name = AttrTp,
      targetType = "const AttributedType *")
AttributedType extends Type[

  @property(type = many-to-one)
  hasModifiedType(QualType qType)[
    $qType = "AttrTp->getModifiedType()"
  ]

  @property(type = many-to-one)
  hasEquivalentType(QualType qType)[
    $qType = "AttrTp->getEquivalentType()"
  ]

  @property(type = existential)
  isMSTypeSpec()[
    "AttrTp->isMSTypeSpec()"
  ]

  @property(type = existential)
  isCallingConv()[
    "AttrTp->isCallingConv()"
  ]

]

@node(name = BinTp,
      targetType = "const BuiltinType *")
BuiltinType extends Type[

  @property(type = existential)
  isInteger()[
    "BinTp->isInteger()"
  ]

  @property(type = existential)
  isSignedInteger()[
    "BinTp->isSignedInteger()"
  ]

  @property(type = existential)
  isUnsignedInteger()[
    "BinTp->isUnsignedInteger()"
  ]

  @property(type = existential)
  isFloatingPoint()[
    "BinTp->isFloatingPoint()"
  ]

  @property(type = existential)
  isPlaceholderType()[
    "BinTp->isPlaceholderType()"
  ]

  @property(type = existential)
  isNonOverloadPlaceholder()[
    "BinTp->isNonOverloadPlaceholderType()"
  ]

]

@node(name = CompTp,
      targetType = "const ComplexType *")
ComplexType extends Type[

  @property(type = many-to-one)
  hasElementType(QualType qTp)[
    $qTp = "CompTp->getElementType()"
  ]

]

@node(name = DecTp,
      targetType = "const DecayedType *")
DecayedType extends Type[

  @property(type = many-to-one)
  hasDecayedType(QualType qTp)[
    $qTp = "DecTp->getDecayedType()"
  ]

  @property(type = many-to-one)
  hasOriginalType(QualType qTp)[
    $qTp = "DecTp->getOriginalType()"
  ]

  @property(type = many-to-one,
  	    cond = "cast<PointerType>(DecTp->getDecayedType())")
  hasPointeeType(QualType qTp)[
    $qTp = "DecTp->getPointeeType()"
  ]

]

@node(name = DepSzVec,
      targetType = "const DependentSizedExtVectorType *")
DependentSizedExtVectorType extends Type[

  /*
   * TODO: Expr (prop && salt) and maybe Loc
   */

  @property(type = many-to-one)
  hasElementType(QualType qTp)[
    $qTp = "DepSzVec->getElementType()"
  ]

]

@node(name = FuncTp,
	targetType = "const FunctionType *")
FunctionType extends Type[

  @property(type = many-to-one)
  hasResultType(QualType resTp)[
    $resTp = "FuncTp->getReturnType()"
  ]

  @property(type = existential)
  isConst()[
    "FuncTp->isConst()"
  ]

  @property(type = existential)
  isVolatile()[
    "FuncTp->isVolatile()"
  ]

  @property(type = existential)
  isRestrict()[
    "FuncTp->isRestrict()"
  ]

  @property(type = many-to-one)
  hasCallResultType(QualType qTp)[
    $qTp = "FuncTp->getCallResultType(*(const_cast<ASTContext*>(Context)))"
  ]

  @property(type = many-to-one)
  hasCallConv(CallingConv callConv)[
    $callConv = "FuncTp->getCallConv()"
  ]

  //TODO: Add some function attributes  (e.g., noReturnAttr)
]

//Ommiting Clang's class FunctionNoPrototype, beacause it does not
//have any useful information

@node(name = FuncPrTp,
      targetType = "const FunctionProtoType *")
FunctionProtoType extends Type[

  /*
   * TODO: C++ 11 features,
   * some attributes that I am not
   * familiar with (e.g., NonThrow, Noexc, etc.)
   * and some qualifiers ...
   * The above may need to be included in the getSalt method.
   */

  @property(type = many-to-many,
  	    targetType = "FunctionProtoType::param_type_iterator",
	    deref = true)
  hasArgument(QualType arg)[
    $arg:start = "FuncPrTp->param_type_begin()"
    $arg:end = "FuncPrTp->param_type_end()"
  ]

  @property(type = many-to-many,
  	    targetType = "FunctionProtoType::exception_iterator",
	    deref = true)
  hasException(QualType exc)[
    $exc:start = "FuncPrTp->exception_begin()"
    $exc:end = "FuncPrTp->exception_end()"
  ]

]

@node(name = InjTpName,
      targetType = "const InjectedClassNameType *")
InjectedClassNameType extends Type[

  /*
   * REVIEW: See if any salt is needed.
   */

  @property(type = many-to-one)
  hasInjectedSpecType(QualType qTp)[
    $qTp = "InjTpName->getInjectedSpecializationType()"
  ]

  @property(type = one-to-one)
  hasInjectedTemplSpecType(TemplateSpecializationType tst)[
    $tst = "InjTpName->getInjectedTST()"
  ]

  @property(type = one-to-one)
  hasDeclaration(CXXRecordDecl decl)[
    $decl = "InjTpName->getDecl()"
  ]

]

@node(name = MemPtr,
      targetType = "const MemberPointerType *")
MemberPointerType extends Type [

  @property(type = many-to-one)
  hasPointeeType(QualType pointeeTp)[
    $pointeeTp = "MemPtr->getPointeeType()"
  ]

  @property(type = many-to-one)
  hasClass(Type _class)[
    $_class = "MemPtr->getClass()"
  ]

]

//Ommiting clang's class ParenType

@node(name = PtrTp,
      targetType = "const PointerType *")
PointerType extends Type[

  @property(type = many-to-one)
  hasPointeeType(QualType ptrTp)[
    $ptrTp = "PtrTp->getPointeeType()"
  ]

]

@node(name = RefTp,
      targetType = "const ReferenceType *")
ReferenceType extends Type[

  @property(type = many-to-one)
  hasPointeeType(QualType ptrTp)[
    $ptrTp = "RefTp->getPointeeTypeAsWritten()"
  ]

  @property(type = existential)
  isInnerRef()[
    "RefTp->isInnerRef()"
  ]
]

@node(name = LRef,
      targetType = "const LValueReferenceType *")
LValueReferenceType extends ReferenceType[

]

@node(name = RRef,
      targetType = "const RValueReferenceType *")
RValueReferenceType extends ReferenceType[

]

@node(name = SumTmpPar,
      targetType = "const SubstTemplateTypeParmType *")
SubstTemplateTypeParmType extends Type [

  @property(type = many-to-one)
  hasReplacementType(QualType repl)[
    $repl = "SumTmpPar->getReplacementType()"
  ]

]

@node(name = TagTp,
      targetType = "const TagType *")
TagType extends Type[

  @property(type = many-to-one)
  hasDecl(TagDecl decl)[
    $decl = "TagTp->getDecl()"
  ]

  @property(type = existential)
  isBeingDefined()[
    "TagTp->isBeingDefined()"
  ]

]

@node(name = RType,
      targetType = "const RecordType *")
RecordType extends TagType[

  @property(type = many-to-one)
  hasDecl(RecordDecl decl)[
    $decl = "RType->getDecl()"
  ]

]

@node(name = EType,
      targetType = "const EnumType *")
EnumType extends TagType[

  @property(type = many-to-one)
  hasDecl(EnumDecl decl)[
    $decl = "EType->getDecl()"
  ]

]

@node(name = TemplSpec,
      targetType = "const TemplateSpecializationType *")
TemplateSpecializationType extends Type[

  @property(type = many-to-many,
            targetType = "TemplateSpecializationType::iterator",
	          ordered = true)
  hasArgument(TemplateArgument arg)[
    $arg:start = "TemplSpec->begin()"
    $arg:end = "TemplSpec->end()"
  ]

  @property(type = many-to-one)
  hasNumberOfArgs(int args)[
    $args = "TemplSpec->getNumArgs()"
  ]

  @property(type = many-to-one,
  	    cond = "TemplSpec->isTypeAlias()")
  hasAliasedType(QualType alias)[
    $alias = "TemplSpec->getAliasedType()"
  ]

  //Temporary I get the decl fdirectly from typename
  //when we are going to have full support of template
  //features (e.g., new stuff from C++11x)
  @property(type = many-to-one,
  	    cond = "TemplSpec->getTemplateName().getAsTemplateDecl() != NULL")
  hasDecl(TemplateDecl decl)[
    $decl = "TemplSpec->getTemplateName().getAsTemplateDecl()"
  ]

]

@node(name = TemplParm,
      targetType = "const TemplateTypeParmType *")
TemplateTypeParmType extends Type[

  @property(type = many-to-one)
  hasDepth(int dep)[
    $dep = "TemplParm->getDepth()"
  ]

  @property(type = many-to-one)
  hasIndex(int ind)[
    $ind = "TemplParm->getIndex()"
  ]

  @property(type = existential)
  isParameterPack()[
    "TemplParm->isParameterPack()"
  ]

  @property(type = many-to-one)
  hasDecl(TemplateTypeParmDecl decl)[
    $decl = "TemplParm->getDecl()"
  ]

]

@node(name = TdefTp,
      targetType = "const TypedefType *")
TypedefType extends Type[

  @property(type = one-to-one)
  hasDecl(TypedefNameDecl decl)[
    $decl = "TdefTp->getDecl()"
  ]

]

/*
 * TODO: DependentNameType,
 *       DependentTemplateSpecializationType::NestedNameSpecifier
 *	 ElaboratedType::NestedNameSpecifier
 * Note: All the above need an entity
 * for NestedNameSpecifier
 */

/* Temporary commenting out this node
 * We need full support of TemplateArguments for 
 * the functional properties to work properly
@node(name = DepNmTp,
      targetType = "const DependentTemplateSpecializationType *")
DependentTemplateSpecializationType extends Type[

  @property(type = many-to-many,
  	    targetType = "DependentTemplateSpecializationType::iterator",
	    ordered = true)
  hasArgument(TemplateArgument arg)[
    $arg:start = "DepNmTp->begin()"
    $arg:end = "DepNmTp->end()"
  ]

  @property(type = many-to-one)
  hasNumberOfArgs(int nArgs)[
    $nArgs = "DepNmTp->getNumArgs()"
  ]

]
*/

/* Temporary commenting out this node (probably needs dependent types)
@node(name = ElTp,
      targetType = "const ElaboratedType *")
ElaboratedType extends Type [

  @property(type = many-to-one)
  hasNamedType(QualType nTp)[
    $nTp = "ElTp->getNamedType()"
  ]

]*/

@node(name = UnTrTp,
      targetType = "const UnaryTransformType *")
UnaryTransformType extends Type[

  @property(type = many-to-one)
  underlyingType(QualType uTp)[
    $uTp = "UnTrTp->getUnderlyingType()"
  ]

  @property(type = many-to-one)
  baseType(QualType bTp)[
    $bTp = "UnTrTp->getBaseType()"
  ]

]

//TODO: UnresolvedUsingType (needs UnresolvedUsingDecl)

@node(name = VecTp,
      targetType = "const VectorType *")
VectorType extends Type[

  @property(type = many-to-one)
  hasElementType(QualType elemTp)[
    $elemTp = "VecTp->getElementType()"
  ]

  @property(type = many-to-one)
  hasNumberOfElems(int num)[
    $num = "VecTp->getNumElements()"
  ]

  //TODO: VectorKind

]

@entity(name = TemplArg,
	targetType = "const TemplateArgument *")
TemplateArgument[

  @constructor(hasUID)

  @property(type = one-to-one)
  hasUID(int id)[
    $id = "getUID(TemplArg)"
  ]

  @property(type = existential)
  isNull()[
    "TemplArg->isNull()"
  ]

  @property(type = many-to-one,
            cond = "TemplArg->getKind() == TemplateArgument::Type")
  hasType(QualType templArg)[
    $templArg = "TemplArg->getAsType()"
  ]
  
  @property(type = many-to-one,
            cond = "TemplArg->getKind() == TemplateArgument::Declaration")
  hasDeclaration(ValueDecl decl)[
    $decl = "TemplArg->getAsDecl()"
  ]

  @property(type = existential)
  isNullPtr()[
    "TemplArg->getKind() == TemplateArgument::NullPtr"
  ]

  @property(type = many-to-one,
  	    cond = "TemplArg->getKind() == TemplateArgument::Integral")
  hasIntegralValue(int val)[
    $val = "TemplArg->getAsIntegral()"
  ]

  //REVIEW: Do we also need the kind of the argument,
  //i.e., Template or TemplateExpansion?
  @property(type = many-to-one,
  	    cond = "TemplArg->getKind() == TemplateArgument::Template || 
	    	    TemplArg->getKind() == TemplateArgument::TemplateExpansion")
  hasTemplateName(TemplateName nm)[
    $nm = "TemplArg->getAsTemplateOrTemplatePattern()"
  ]

   /*
    * TODO: Expression,
    *       Pack
    *       And any other property
    *       that may be useful.
    */
]

@entity(name = TempName,
	targetType = "const TemplateName")
TemplateName[

  @constructor(hasUID)

  @property(type = one-to-one)
  hasUID(int uid)[
    $uid = "getUID(TempName)"
  ]

  /*@property(type = many-to-one,
  	    cond = "TempName.getAsTemplateDecl() != NULL")
  hasDecl(TemplateDecl decl)[
    $decl = "TempName.getAsTemplateDecl()"
  ]*/

  @property(type = many-to-many,
            targetType = "OverloadedTemplateStorage::iterator",
	    deref = true,
  	    cond = "TempName.getKind() == TemplateName::OverloadedTemplate")
  hasOverloadedTemplates(NamedDecl decl)[
    $decl:start = "TempName.getAsOverloadedTemplate()->begin()"
    $decl:end = "TempName.getAsOverloadedTemplate()->end()"
  ]

]

@entity(name = DepTempName,
	targetType = "const DependentTemplateName *")
DependentTemplateName[

  @constructor(hasUid)

  @property(type = one-to-one)
  hasUid(int uid)[
    $uid = "getUID(DepTempName)"
  ]

  @property(type = many-to-one)
  hasQualifier(NestedNameSpecifier spec)[
    $spec = "DepTempName->getQualifier()"
  ]

  @property(type = many-to-one,
  	    cond = "DepTempName->isIdentifier()")
  hasIdentifier(string identifier)[
    $identifier = "DepTempName->getIdentifier()->getName()"
  ]

  /*
   * TODO: Make an enumeration for the operators,
   * now they are just the enumerators indedx.
   */
  @property(type = many-to-one,
  	    cond = "DepTempName->isOverloadedOperator()")
  hasOverloadedOperator(int op)[
    $op = "DepTempName->getOperator()"
  ]

]

@entity(name = QualTempName,
        targetType = "const QualifiedTemplateName *")
QualifiedTemplateName[

  @constructor(hasUID)

  @property(type = one-to-one)
  hasUID(int uid)[
    $uid = "getUID(QualTempName)"
  ]

  @property(type = many-to-one)
  hasQualifier(NestedNameSpecifier spec)[
    $spec = "QualTempName->getQualifier()"
  ]

  /*@property(type = many-to-one)
  hasDecl(TemplateDecl decl)[
    $decl = "QualTempName->getDecl()"
  ]*/

]

@entity(name = SubstTempTempParm,
	targetType = "const SubstTemplateTemplateParmStorage *")
SubstTemplateTemplateParmStorage [

  @constructor(hasUID)

  @property(type = one-to-one)
  hasUID(int uid)[
    $uid = "getUID(SubstTempTempParm)"
  ]

  /*@property(type = many-to-one)
  hasParameter(TemplateTemplateParmDecl param)[
    $param = "SubstTempTempParm->getParameter()"
  ]*/

  @property(type = many-to-one)
  hasReplacement(TemplateName repl)[
    $repl = "SubstTempTempParm->getReplacement()"
  ]

]

@entity(name = QType,
	targetType = "QualType")
QualType[

  @constructor(uid)

  @property(type = one-to-one)
  uid(int id)[
    $id = "getUID(QType)"
  ]

  @property(type = many-to-one)
  hasType(Type tp)[
    $tp = "QType.getTypePtr()"
  ]

  @property(type = many-to-one)
  toString(string str)[
    $str = "QType.getAsString(Context->getPrintingPolicy())"
  ]

  @property(type = many-to-one)
  singleDesugaredType(QualType desugared)[
    $desugared = "QType.getSingleStepDesugaredType(*Context)"
  ]

  @property(type = many-to-one,
  	    cond = "QType.hasLocalQualifiers()")
  hasLocalQualifiers(Qualifiers quals)[
    $quals = "QType.getLocalQualifiers()"
  ]
]

/*
 * Since all these information is available
 * directly from clang API, I don't create
 * an enumeration.
 */
@entity(name = Quals,
	targetType = "Qualifiers")
Qualifiers[

  @constructor(toString)

  @property(type = many-to-one)
  toString(string str)[
    $str = "Quals.getAsString()"
  ]

  @property(type = existential)
  hasConst()[
    "Quals.hasConst()"
  ]

  @property(type = existential)
  hasVolatile()[
    "Quals.hasVolatile()"
  ]

  @property(type = existential)
  hasRestrict()[
    "Quals.hasRestrict()"
  ]

  //REVIEW: do we need address space and
  //Objective-C qualifiers?
]
//end Types and Qualified Types
