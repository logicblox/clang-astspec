package listener;

import errormessage.ErrorMessage;
import errormessage.ErrorReporter;
import java.util.List;
import java.util.Set;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.misc.NotNull;
import org.jgrapht.DirectedGraph;
import org.jgrapht.alg.CycleDetector;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import parser.ASTSpecBaseListener;
import parser.ASTSpecParser;
import symboltable.ASTNode;
import symboltable.Constructor;
import symboltable.ContainsProperties;
import symboltable.Entity;
import symboltable.EntityProperty;
import symboltable.EnumProperty;
import symboltable.Enumeration;
import symboltable.ExistentialProperty;
import symboltable.Property;
import symboltable.ExprForASTElement;
import symboltable.IntProperty;
import symboltable.NodeProperty;
import symboltable.StringProperty;
import symboltable.SymbolTable;
import symboltable.Util;
import static symboltable.Util.stringToType;
import visitor.forproperties.PropertyVisitor;

/**
 *
 * @author kostasferles
 */
public class GatherProperties extends ASTSpecBaseListener {
    
    private ErrorReporter reporter = ErrorReporter.getInstance();
    
    private SymbolTable symTable = SymbolTable.getInstance();
    
    private ContainsProperties currElem;
    
    private Property currProperty;
    
    private List<Token> propertiesForConstructor;
    
    private DirectedGraph<ContainsProperties, DefaultEdge> consGraph = new DefaultDirectedGraph<>(DefaultEdge.class);
    
    private AddGraphEdgeVisitor visitor = new AddGraphEdgeVisitor();
    
    private Property createPropertyFromType(String propertyName, ContainsProperties parent, String type, String rangeType, String rangeValName) throws ErrorMessage{
        Property.Type pType = stringToType(type);
        switch(rangeType){
            case "":
                return new ExistentialProperty(propertyName, parent);
            case "string":
                return new StringProperty(propertyName, parent, pType, rangeValName);
            case "int":
                return new IntProperty(propertyName, parent, pType, rangeValName);
            default:
                ASTNode node = symTable.getNode(rangeType);
                if(node != null){
                    return new NodeProperty(propertyName, parent, pType, rangeValName, node);
                }

                Enumeration enumeration = symTable.getEnumeration(rangeType);
                if(enumeration != null){
                    return new EnumProperty(propertyName, parent, pType, rangeValName, enumeration);
                }
                
                Entity en = symTable.getEntity(rangeType);
                if(en != null){
                    return new EntityProperty(propertyName, parent, pType, rangeValName, en);
                }
                
                throw new ErrorMessage('\'' + rangeType + "' unknown node type, entity type or enumeration");
                 
        }
    }
       
    private String cleanReference(String ref){
        return ref.substring(1, ref.length());
    }
    
    private String cleanIterReferences(String ref){
        return ref.substring(1, ref.lastIndexOf(':'));
    }
    
    private class AddGraphEdgeVisitor implements PropertyVisitor{

        private void addEdge(ContainsProperties from, ContainsProperties to){
            consGraph.addVertex(from);
            consGraph.addVertex(to);
            consGraph.addEdge(from, to);
        }
        
        @Override
        public void visit(StringProperty sp) {
            
        }

        @Override
        public void visit(EnumProperty enp) {
            
        }
        
        @Override
        public void visit(IntProperty ip){
            
        }
        
        @Override
        public void visit(ExistentialProperty ep){
            
        }

        @Override
        public void visit(NodeProperty np) {
            ContainsProperties pNode = np.getParent();
            ASTNode rangeNode = np.getRangeNode();
            addEdge(pNode, rangeNode);
        }
        
        @Override
        public void visit(EntityProperty ep){
            ContainsProperties pNode = ep.getParent();
            Entity rangeEn = ep.getRangeEntity();
            addEdge(pNode, rangeEn);
        }
        
    }
    
    @Override
    public void enterNode_entity_definition(@NotNull ASTSpecParser.Node_entity_definitionContext ctx) {
        String elemName = ctx.name.getText();
        if(ctx.ANNOTATION_NODE() != null){
            this.currElem = this.symTable.getNode(elemName);
        }
        else{
            this.currElem = this.symTable.getEntity(elemName);
        }
        assert this.currElem != null;
    }
    
    @Override
    public void exitNode_entity_definition(@NotNull ASTSpecParser.Node_entity_definitionContext ctx){
        assert this.currElem != null;
        if(this.propertiesForConstructor != null){
            Constructor cons;
            Constructor supersCons = this.currElem.getConstructor();
            if(supersCons != null){
                cons = new Constructor(currElem, supersCons);
            }
            else{
                cons = new Constructor(this.currElem);
            }
            Token errToken = ctx.node_definition_content_element().constructor_definition(0).start;
            for(Token t : this.propertiesForConstructor){
                String propertyName = t.getText();
                Property p = this.currElem.getProperty(propertyName);
                if(p == null){
                    reporter.reportError(t, "undefined reference to property '" + propertyName + '\'');
                    break;
                }
                
                try{
                    p.accept(visitor);
                    cons.insertProperty(p);
                }
                catch(ErrorMessage er){
                    reporter.reportError(errToken, er.getMessage());
                    cons = null;
                    break;
                }
            }
            if(cons != null){
                try{
                    this.currElem.insertConstructor(cons);
                }
                catch(ErrorMessage er){
                    reporter.reportError(errToken, er.getMessage());
                }
            }
            this.propertiesForConstructor = null;
        }
        else {
            Token errToken = ctx.name;
            if(this.currElem.getConstructor() == null){
                reporter.reportError(errToken, "node '" + errToken.getText() + "' has no constructor");
            }
        }
        this.currElem = null;
    }
    
    @Override
    public void enterProperty_definition(@NotNull ASTSpecParser.Property_definitionContext ctx) {
        
        assert currElem != null;
        
        String propertyType = ctx.PR_TYPE_VALUE().getText();
        Token propertyName = ctx.IDENTIFIER().getSymbol();
        ASTSpecParser.ParameterContext param = ctx.parameter();
        Token paramType;
        Token paramIdent;
        if(param == null){
            if(!propertyType.equals("existential")){
                this.reporter.reportError(propertyName, "missing parameter for non existential property '" + propertyName.getText() + "'");
                return;
            }
            paramType = null;
            paramIdent = null;
        }
        else{
            paramType = param.type;
            paramIdent = param.name;
        }
        
        String rangeValueName;
        if(paramIdent != null)
            rangeValueName = paramIdent.getText();
        else
            rangeValueName = "";
        
        String paramTypeStr;
        
        if(paramType != null)
            paramTypeStr = paramType.getText();
        else
            paramTypeStr = "";
        
        Property pr;
        try{
            pr = createPropertyFromType(propertyName.getText(), this.currElem, propertyType, paramTypeStr, rangeValueName);
        }
        catch(ErrorMessage er){
            pr = null;
            this.reporter.reportError(paramType, er.getMessage());
        }
        
        if(pr != null){
            
            if(ctx.targetType != null){
                pr.setTargetLangType(Util.unwrapString(ctx.targetType.getText()));
            }
            
            if(ctx.cond != null){
                pr.setCond(Util.unwrapString(ctx.cond.getText()));
            }
            
            Token nDeref = ctx.needsDeref; 
            if(nDeref != null && nDeref.getText().equals("true")){
                pr.setDeref();
            }
            
            Token order = ctx.order;
            if(order != null && order.getText().equals("true")){
                pr.setOrdered();
            }
            
            try{
                this.currElem.insertProperty(pr);
            }
            catch(ErrorMessage er){
                pr = null;
                reporter.reportError(propertyName, er.getMessage());
            }
        }
        
        this.currProperty = pr;
    }
    
    @Override
    public void exitProperty_definition(@NotNull ASTSpecParser.Property_definitionContext ctx){
        if(this.currProperty != null){
            try{
                this.currProperty.checkCommandsForProperty();
            }
            catch(ErrorMessage er){
                this.reporter.reportError(ctx.IDENTIFIER().getSymbol(), er.getMessage());
            }
            this.currProperty = null;
        }
    }
    
    @Override
    public void exitClang_code_for_property(@NotNull ASTSpecParser.Clang_code_for_propertyContext ctx) {
        if(this.currProperty != null){
            Token errToken = null;
            try{
                Token ref = ctx.ref,
                      refStart = ctx.refStart,
                      refEnd = ctx.refEnd;

                String codeForPr = Util.unwrapString(ctx.STRING_LITERAL().getSymbol().getText());
                ExprForASTElement code = new ExprForASTElement(codeForPr);
                String valuePropertyName;

                if(ref != null){
                    errToken = ref;
                    valuePropertyName = cleanReference(ref.getText());
                    currProperty.setCommandForValue(valuePropertyName, code);
                }
                else if(refStart != null){
                    errToken = refStart;
                    valuePropertyName = cleanIterReferences(refStart.getText());
                    currProperty.setCommandStart(valuePropertyName, code);
                }
                else if(refEnd != null){
                    errToken = refEnd;
                    valuePropertyName = cleanIterReferences(refEnd.getText());
                    currProperty.setCommandEnd(valuePropertyName, code);
                }
                else{
                    valuePropertyName = "";
                    errToken = ctx.start;
                    /*
                     * existential property
                     */
                    if(this.currProperty.getType() != Property.Type.Existential){
                        this.reporter.reportError(errToken, "missing reference to value for property '" + currProperty.getName() + "'");
                    }
                    else{
                        currProperty.setCommandForValue(valuePropertyName, code);
                    }
                }
            }
            catch(ErrorMessage er){
                this.reporter.reportError(errToken, er.getMessage());
            }
        }
    }
    
    @Override
    public void exitConstructor_definition(@NotNull ASTSpecParser.Constructor_definitionContext ctx){
        propertiesForConstructor = ctx.identifier_list().ids;
    }
    
    @Override
    public void exitSpecification(@NotNull ASTSpecParser.SpecificationContext ctx){
        CycleDetector<ContainsProperties, DefaultEdge> detectCyc = new CycleDetector<>(consGraph);
        
        Set<ContainsProperties> cycles = detectCyc.findCycles();
        if(!cycles.isEmpty()){
            String errorMessage = "found cycles in constructors containing the following node(s): " + cycles.toString();
            this.reporter.reportError(ctx.start, errorMessage);
        }
        
    }
}
