package listener;

import org.antlr.v4.runtime.misc.NotNull;
import parser.ASTSpecBaseListener;
import parser.ASTSpecParser;
import symboltable.SymbolTable;
import symboltable.Util;

/**
 *
 * @author kostasferles
 */
public class GatherAuxCppCodeSegments extends ASTSpecBaseListener{

    private SymbolTable table = SymbolTable.getInstance();
    
    @Override
    public void enterLang_annotations(@NotNull ASTSpecParser.Lang_annotationsContext ctx){
        String annotation = ctx.LANG_ANNOTATIONS().getText();
        String codeSeg = ctx.CODE_SEGMENT().getText();
        codeSeg = Util.unwrapString(codeSeg);
        switch(annotation){
            case "@header":
                table.insertHeader(codeSeg);
                break;
            case "@members":
                table.insertMembers(codeSeg);
                break;
            default:
                assert false;
        }
    }
}
