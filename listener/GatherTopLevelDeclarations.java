package listener;

import errormessage.ErrorMessage;
import errormessage.ErrorReporter;
import java.util.List;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.TerminalNode;
import parser.ASTSpecBaseListener;
import parser.ASTSpecParser;
import symboltable.ASTNode;
import symboltable.ContainsProperties;
import symboltable.Entity;
import symboltable.Enumeration;
import symboltable.SymbolTable;
import symboltable.Util;

/**
 *
 * @author kostasferles
 */
public class GatherTopLevelDeclarations extends ASTSpecBaseListener{
    
    SymbolTable symbolTable = SymbolTable.getInstance();
    
    ErrorReporter reporter = ErrorReporter.getInstance();

    @Override
    public void enterSpecification(@NotNull ASTSpecParser.SpecificationContext ctx){
        this.symbolTable.setClangSpecName(ctx.IDENTIFIER().getText());
    }
    
    @Override
    public void exitEnumeration_definition(@NotNull ASTSpecParser.Enumeration_definitionContext ctx){
        Token enumName = ctx.name;
        String name = enumName.getText();
        Enumeration enumeration = new Enumeration(name);
        List<Token> enumerators = ctx.enumerator;
        try{
            this.symbolTable.insertEnum(enumeration);
            if(ctx.prefix != null){
                enumeration.setPrefix(Util.unwrapString(ctx.prefix.getText()));
            }
            for(Token enumerator : enumerators)
                enumeration.addEnumerator(enumerator.getText());
        }
        catch(ErrorMessage er){
            reporter.reportError(enumName, er.getMessage());
        }
    }
    
    @Override
    public void exitNode_entity_definition(@NotNull ASTSpecParser.Node_entity_definitionContext ctx){
        Token elemName = ctx.name;
        Token baseElemName = ctx.baseName;
        TerminalNode nodeAnnot = ctx.ANNOTATION_NODE();
        
        ContainsProperties baseElem = null;
        if(baseElemName != null){
            String baseElemNameStr = baseElemName.getText();
            if(nodeAnnot != null){
                baseElem = this.symbolTable.getNode(baseElemNameStr);
                if(baseElem == null){
                    this.reporter.reportError(baseElemName, baseElemNameStr + " is undefined");
                    return;
                }
            }
            else{
                baseElem = this.symbolTable.getEntity(baseElemNameStr);
                if(baseElem == null){
                    this.reporter.reportError(baseElemName, baseElemNameStr + " is undefined");
                    return;
                }
            }
                
        }
        
        ContainsProperties newElem;
        String elemNameStr = elemName.getText();
        String thisName = ctx.thisName.getText();
        if(baseElem == null){
            newElem = nodeAnnot != null ? new ASTNode(elemNameStr, thisName) : new Entity(elemNameStr, thisName);
        }
        else{
            newElem = nodeAnnot != null ? new ASTNode(elemNameStr, thisName, (ASTNode)baseElem) 
                                        : new Entity(elemNameStr, thisName, (Entity)baseElem);
        }
        
        if(ctx.targetType != null){
            String targetType = ctx.targetType.getText();
            targetType = Util.unwrapString(targetType);
            newElem.setTargetLangType(targetType);
        }
        
        if(ctx.cache != null){
            if(ctx.cache.getText().equals("true")){
                if(nodeAnnot != null){
                    ((ASTNode) newElem).setNeedsCache();
                }
                //TODO: If it's an entity report a warning
            }
        }
        
        try{
            if(nodeAnnot != null){
                this.symbolTable.insertNode((ASTNode) newElem);
            }
            else{
                this.symbolTable.insertEntity((Entity) newElem);
            }
        }
        catch(ErrorMessage er){
            reporter.reportError(elemName, er.getMessage());
        }
    }

}
